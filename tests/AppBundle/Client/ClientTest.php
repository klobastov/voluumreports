<?php

namespace Tests\AppBundle\Client;

use Requests;
use Requests_Response;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ClientTest extends WebTestCase
{
    public function testRequestForMyselfShouldBeSuccessButAuthFailed()
    {
        $response = Requests::get('http://voluumreports.dev/',[],['connect_timeout'=>30,'timeout'=>30]);

        $this->assertInstanceOf(Requests_Response::class, $response);
        $this->assertFalse($response->success);
        $this->assertEquals(401, $response->status_code);
    }
}
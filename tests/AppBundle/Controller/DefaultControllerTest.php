<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{
    public function testIndexWithoutAuth()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');

        $this->assertEquals(401, $client->getResponse()->getStatusCode());
        $this->assertContains('Enter secret', $crawler->filter('#content label')->text());
    }
}

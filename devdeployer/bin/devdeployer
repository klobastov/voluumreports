#!/bin/bash

script=`realpath $0`
base_dir=`dirname ${script}`
env_dir="$base_dir/../../"

settings_file="../config/settings"
if [ -f ${base_dir}'/'${settings_file} ]; then
   . ${base_dir}'/'${settings_file}
else
    #-----------------------------#----------------------------------------------------------------------------------#
    # default [example] structure #                                                                                  #
    # - home                      # Home folder                                                                      #
    #   - {deployer_name}         # User for development deploy                                                      #
    #       - {project_name}      # Inner files: devdeployer folder and regular skeleton of project                  #
    #           - devdeployer     # Inner files: bin, compose, config folders                                        #
    #       - app_environment     #Inner files: devdeployer.yml, .env.devdeployer, files for services with Dockerfile#
    #-----------------------------#----------------------------------------------------------------------------------#
    dd_user='deployer'
    dd_app_name='dd'
    dd_root="/home/$dd_user"
    dd_self_name="project/devdeployer"
    dd_env_deny_list[1]='COMPOSE_PROJECT_NAME'
    dd_project_folders[1]='project'
    dd_project_folders_enabled=( 1 )
fi

for args in "$@"
do
    case ${args} in
        -l|--list) list=1
        shift;;
        -p|--prepare) prepare=1                 #agregate .env from each folder
        shift;;
        -u=*|--update=*) update="${args#*=}"    #update .env from folder by position number
        shift;;
        -b=*|--backup=*) backup="${args#*=}"    #create backup of .env file by position number
        shift;;
        -g|--generate) generate=1               #generate or regenerate main .env, .compose.args, change project name
        shift;;
        -i|--init) init=1                       #init project(via another script) custom project settings
        shift;;
        -e=*|--env=*) environment="${args#*=}"  #this var used on init project
        shift;;
        --up) up=1                              #compose up for all .compose.args
        shift;;
        --start) start=1                        #compose start for all .compose.args
        shift;;
        --stop) stop=1                          #docker stop for all .compose.args
        shift;;
        --build) build=1                        #docker build for all .compose.args
        shift;;
        --build-force) build_force=1            #docker build force for all .compose.args
        shift;;
        --pull) pull=1                          #docker pull for all .compose.args
        shift;;
        --remove) remove=1                      #remove all containers from .compose.args
        shift;;
        *) echo 'Unknown option' exit 4 ;;
    esac
done

if [[ -n $1 ]]; then
    tail -1 $1
fi

RED='\033[0;31m'
NC='\033[0m' # No Color
GREEN='\033[0;32m'
CYAN='\033[0;36m'

function validateProjectFolder(){
    folder=$1
    if [ ! -d ${dd_root}'/'${folder} ]; then
        echo " ${folder} not exist in ${dd_root}/${folder}/${folder}"
        exit 1
    fi

    compose_file=$2
    if [ ! -f ${dd_root}'/'${folder}'/'${compose_file} ]; then
        echo " ${compose_file} not exist in ${dd_root}/${folder}/${compose_file}"
        exit 2
    fi

    env_file=$3
    if [ ! -f ${dd_root}'/'${folder}'/'${env_file} ]; then
        echo " ${env_file} not exist in ${dd_root}/${folder}/${env_file}"
        exit 3
    fi
}

function validateEnvLine(){
    line=$1
    deny_word_count=${#dd_env_deny_list[@]}
    deny_word_iterator=1
    while [ ${deny_word_iterator} -le ${deny_word_count} ]
    do
        deny_word=${dd_env_deny_list[$deny_word_iterator]}
        if [[ "$line" = *${deny_word}* ]]; then
            echo -n 'false'
        else
            echo -n 'true'
        fi
        deny_word_iterator=$((deny_word_iterator+1))
    done
}

function aggregateEnvFileProjectFolder(){
    folder=$1
    if [ ${prepare:-0} = 1 ] || [ ${update:-0} != 0 ]; then
        echo -en ${CYAN}' - Agregated ('
        if [ -e "$dd_root/$dd_self_name/config/.env.$folder" ]; then
            rm "$dd_root/$dd_self_name/config/.env.$folder"
        fi

        if [ ! -e "$dd_root/$dd_self_name/config/.env.$folder" ]; then
            echo -n ".env.$folder [deleted] "
        fi
        while read -r line
        do
            valid_line=$( validateEnvLine ${line} )
            if [[ "$valid_line" = *'false'* ]]; then
                continue
            else
                if [ ! -z "$line" ]; then
                    echo "$line" >> "$dd_root/$dd_self_name/config/.env.$folder"
                fi
            fi
        done < ${dd_root}'/'${folder}'/.env.devdeployer'
        if [  -e "$dd_root/$dd_self_name/config/.env.$folder" ]; then
            echo -n ".env.$folder [created]"
        fi
        echo -en ')'${NC}
    fi
}

function removeGeneratedConfigs(){
    if [ ${generate:-0} = 1 ]; then
        echo -en ${CYAN}'Remove configs ('
        if [ -e "$env_dir/.env" ]; then
            rm "$env_dir/.env"
        fi

        if [ ! -e "$env_dir/.env" ]; then
            echo -n '.env [deleted] '
        fi

        if [ -e "$dd_root/$dd_self_name/compose/.order" ]; then
            rm "$dd_root/$dd_self_name/compose/.order"
        fi

        if [ ! -e "$dd_root/$dd_self_name/compose/.order" ]; then
            echo -n '.order [deleted] '
        fi
        echo -en ')'${NC}
    fi

    if [ ${environment:-0} = 'args' ]; then
        if [ -e "$dd_root/$dd_self_name/compose/.order" ]; then
            rm "$dd_root/$dd_self_name/compose/.order"
        fi
    fi
}

function generateMainEnvFileWith(){
    folder=$1
    if [ ${generate:-0} = 1 ]; then
        echo -en ${CYAN}' - Create config ('
        if [ -e "$dd_root/$dd_self_name/config/.env.$folder" ]; then
            echo "# $folder" >> "$env_dir/.env"
            while read -r line
            do
                echo "$line" >> "$env_dir/.env"
            done < "$dd_root/$dd_self_name/config/.env.$folder"

            echo -n ' .env [created] '
        fi
        echo -en ')'${NC}
    fi
}

function listAvailableProjectFolders(){
    if [ ${list:-0} = 1 ]; then
        echo -e "Application:\t$dd_app_name"
        echo -e "Self name:\t$dd_self_name"
        echo -e "Folders:\t${dd_project_folders[*]}"
        echo -e "Enabled:\t${CYAN}${dd_project_folders_enabled[*]}${NC}"
        echo -e "Deny list:\t${dd_env_deny_list[*]}"
        echo -e '\r'
        folders_count=${#dd_project_folders[@]}
        folder_iterator=0
        while [ ${folder_iterator} -lt ${folders_count} ]
        do
            folder_iterator=$((folder_iterator+1))
            folder=${dd_project_folders[$folder_iterator]}
            if [[ ! ${dd_project_folders_enabled[*]} =~ "$folder_iterator" ]]; then
                echo -e ${RED}'[disabled]'${NC}' | '${CYAN}${folder_iterator}${NC}' | '${folder}
                continue
            fi
            echo -e ${GREEN}'[ enabled]'${NC}' | '${CYAN}${folder_iterator}${NC}' | '${folder}
        done
        echo -e '\r'
        exit 0
    fi
}

function updateEnvFileProjectFolder(){
    if [ ${update:-0} != 0 ]; then
        folder_id=${update}
        folder=${dd_project_folders[$folder_id]}
        echo -e "${GREEN}Update ready${NC}: ${CYAN}$dd_root/$dd_self_name/config/.env.$folder${NC}"
        aggregateEnvFileProjectFolder ${folder} > /dev/null
        exit 0
    fi
}

function backupEnvFileProjectFolder(){
    if [ ${backup:-0} != 0 ]; then
        folder_id=${backup}
        folder=${dd_project_folders[$folder_id]}
        cp "$dd_root/$dd_self_name/config/.env.$folder" "$dd_root/$dd_self_name/config/bcp.$folder"
        echo -e "${GREEN}Backup ready${NC}: ${CYAN}$dd_root/$dd_self_name/config/bcp.$folder${NC}"
        exit 0
    fi
}

function initDockerParams(){
    if [ ${generate:-0} = 1 ]; then
        echo "# init params" >> "$env_dir/.env"
        echo "COMPOSE_PROJECT_NAME=$dd_app_name" >> "$env_dir/.env"
    fi
}

function initWrapper(){
    if [ ${generate:-0} = 1 ] || [ ${environment:-0} = 'args' ]; then
        echo -en ${CYAN}' - Init wrapper ('
        echo -n "-f $dd_root/$dd_self_name/compose/docker-compose.yml " >> "$dd_root/$dd_self_name/compose/.order"
        echo -en ')'${NC}
    fi
}

function afterInitWrapper(){
    if [ ${generate:-0} = 1 ] || [ ${environment:-0} = 'args' ]; then
        echo -en ${CYAN}' - Init overrider ('
        echo -n "-f $dd_root/$dd_self_name/compose/dev.yml " >> "$dd_root/$dd_self_name/compose/.order"
        echo -en ')'${NC}
    fi
}

function upContainers(){
    if [ ${up:-0} = 1 ]; then
        docker-compose $(head -n 1 "$dd_root/$dd_self_name/compose/.order") up -d
    fi
}

function buildContainers(){
    if [ ${build:-0} = 1 ]; then
        docker-compose $(head -n 1 "$dd_root/$dd_self_name/compose/.order") build --force-rm
    fi
}

function buildForceContainers(){
    if [ ${build_force:-0} = 1 ]; then
        docker-compose $(head -n 1 "$dd_root/$dd_self_name/compose/.order") build --no-cache --force-rm
    fi
}

function pullContainers(){
    if [ ${pull:-0} = 1 ]; then
        docker-compose $(head -n 1 "$dd_root/$dd_self_name/compose/.order") pull
    fi
}

function startContainers(){
    if [ ${start:-0} = 1 ]; then
        docker-compose $(head -n 1 "$dd_root/$dd_self_name/compose/.order") start
    fi
}

function stopContainers(){
    if [ ${stop:-0} = 1 ]; then
        docker-compose $(head -n 1 "$dd_root/$dd_self_name/compose/.order") stop
    fi
}

function removeContainers(){
    if [ ${remove:-0} = 1 ]; then
        docker rm $(docker container ls --filter "name=$dd_app_name*" -aq)
    fi
}

saveArgsProjectFolder(){
    if [ ${generate:-0} = 1 ] || [ ${environment:-0} = 'args' ]; then
        folder=$1
        echo -n "-f $dd_root/$folder/devdeployer.yml " >> "$dd_root/$dd_self_name/compose/.order"
    fi
}

echo -e '\r'
echo -e "${GREEN}Path${NC}:\t${CYAN}$dd_root ${NC}"

removeGeneratedConfigs
initDockerParams

updateEnvFileProjectFolder
backupEnvFileProjectFolder

echo -e '\r'
echo -ne "${GREEN}Order${NC}:\t${RED}wrapper"
initWrapper
folders_count=${#dd_project_folders[@]}
folder_iterator=0
while [ ${folder_iterator} -lt ${folders_count} ]
do
    folder_iterator=$((folder_iterator+1))
    folder=${dd_project_folders[$folder_iterator]}

    if [[ ${dd_project_folders_enabled[*]} != *${folder_iterator}* ]]; then
        continue
    fi

    echo -ne " ${NC}> ${RED}"${folder}${NC}
    validateProjectFolder ${folder} 'devdeployer.yml' '.env.devdeployer'
    saveArgsProjectFolder ${folder}
    aggregateEnvFileProjectFolder ${folder}
    generateMainEnvFileWith ${folder}
done

echo -en " ${NC}> ${RED}override ${NC}"
afterInitWrapper
echo -e '\r'

listAvailableProjectFolders

removeContainers
buildContainers
buildForceContainers
pullContainers
startContainers
stopContainers
upContainers

/bin/bash "${base_dir}/../config/scenario" ${init:-0} ${environment:-0} ${dd_app_name}
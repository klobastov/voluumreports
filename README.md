# Документация по VoluumReports
## Cтруктура файлов
* app/config настройки компонентов фреймворка, кастомных настроек нет
* app/resources исходники фронтенда, css\js\отображения
* devdeployer скрипт для развёртки проекта в dev окружения
* docker набор настроек, необходимых для создания dev окружения
* reports отчёты, формируемые при запуске PHPUnit тестов - покрытие, лог
* src PHP исходники 
* tests PHP тесты
* web webroot-проекта, содержит только входной PHP скрипт, собранный через gulp фронтенд, robots.txt, favicon.ico

## Конфиг
### Демо доступ
Инструкция по генерации ключей https://doc.voluum.com/en/voluum_api_docs.html

Креденшилы для входа в демо аккаунт https://developers.voluum.com/#!/authentication
"email":"voluumdemo@voluum.com","password":"1qaz!QAZ"

### config.yml
parameters:
	secrets: - массив паролей, с которыми можно войти на сайт
framework:
	дефолтный список парамтров

### config_prod.yml, config_dev.yml, config_test.yml
Конфиги для окружений не имеют кастомных настроек
Везде выключена doctrine
В проекте нет БД

### security.yml
В качестве авторизации используется user_token_provider
Токен формируется не проектный, а создается на стороне волуума
Включает механизм проверки токена, для автономной переавторизации, в случае протухания сессии,
но этот механизм отключен, в силу долгого ожидания ответа от API.

### services.yml
Все сервисы используют автовайринг
Необходимые сервисы для компонента security указаны вручную с необходимым списком зависимостей

## Как обновлять
Пушить в ветку master, через CI сервер
	* перейти в раздел Deploy и выбрать сервер, staging или prod окружение
	* в открывшемся списке окружений выбрать нужное, и в столбце Actions нажать на deploy
	* выбрать: создать новый релиз на основе выбранного коммита, или использовать существующий, что бы откатиться до него
	* нажать Start deployment

## Как развернуть
	* склонировать приложение и запомнить имя папки
	* добавить симлинк внутри приложения из `devdeployer/bin/devdeployer` в `bin/devdeployer`
	* создать конфигурационный файл `settings` из файла `settings.example` в каталоге `devdeployer/config`
	* настроить конфиг, изменить имя пользователя и пути, они зависят от имени папки, в которой лежит проект
	* `./bin/devdeployer -p` 
	* `./bin/devdeployer -g` 
	* `./bin/devdeployer --up` 
	* `./bin/devdeployer -i -e=init` 
	* `./bin/devdeployer -i -e=network` 

Пример найстройки devdeployer
```
dd_app_name='vol'
dd_env_deny_list[1]='COMPOSE_PROJECT_NAME'

dd_user='k.lobastov'                        # User, folder in /home/${dd_user}
dd_root="/home/${dd_user}/git"              # Path to folder with {application}, without trailing slash
dd_self_name='voluumreports/devdeployer'    # Path to 'devdeployer' folder relative from ${dd_root}, without trailing slash
dd_project_folders[1]='voluumreports'       # {application} name of folder, placed into ${dd_root}
dd_project_folders_enabled=( 1 )            # List of enabled folders for composer without comma with space
```

## Дополнительно
Проект использует API сервиса voluum.com
Основная задача расширить функционал основного сайта для построения отчёта.
Проделанные работы: разработан инструмент мапинга ответов API, с последующим отображением в таблицу.
Основный прицнип: декларативность моделей и отсутсвие "магических" констант

Стек 
* получаем запрос от веб интерфейса, построенный на основе фильтров и группировок
* формируем на основе полученных данных запрос к API и отправляе его
* полученный результат проверяем на его состояние, ожидается получение конкертной модели отчёта, либо ошибки
* полученной состояние ответа мы подготавливаем для дальнейшего отображения, путём обезки ненужных полей, на случай обработки данных или их сохранения\кэширования
* мапим в объекты
* полученные объекты отображаем

Модель:
Клас содержащий публичные свойства = поля ответа API
Метод getMappedFields, предопределяющий какое имеено сосотяние ожидается в конкретном поле ответа, коллекция это схем или просто схема
Методы инициализирующие объекты, обрабатывающие поля
Метод getLayer позволяющий определить родителя\детей конкретной модели для дальнейшего понимания, нужно ли уходить глубже для мапинга и обработки состояния
Метод мапинга и закрытый конструктор

Добавление отчётов происходит путём добавления модели, и определения её в файле MappedModels(/src/AppBundle/Scheme/MappedModels.php) с уникальным идентификатором

Возможно перпеопределние каких-то моделей, это используется в классах Row, т.е. самой замапленой строке.
Сам отчёт не знает отчётом чего он является, кампании или бренда и т.д. потому что это можно определить только по состоянию полученных данных, т.е. коллекции схем
Только объекты Row имеют понятие о своём содержимом

### Пример модели главного отчёта для вызова ресурса /report [GET] в API 
```<?php
   
   namespace AppBundle\Model;
   
   use AppBundle\Scheme\Mapping\MappedScheme;
   use AppBundle\Scheme\Nesting\LayerNestingScheme;
   use AppBundle\Scheme\Types\CollectionType;
   use AppBundle\Scheme\Types\FieldBasedCollection;
   use AppBundle\Scheme\Types\SchemeType;
   
   class Report extends LayerNestingScheme
   {
       public $rows;
       public $offset;
       public $limit;
       public $totalRows;
       public $totals;
       public $messages;
   
       public static function getMappedFields(): array
       {
           return [
               'rows' => FieldBasedCollection::class,
               'messages' => CollectionType::class,
               'totals' => SchemeType::class,
           ];
       }
   
       public static function initRows()
       {
           return new FieldBasedCollection(ReportRow::class);
       }
   
       public static function initMessages()
       {
           return new CollectionType(ReportMessage::class);
       }
   
       public static function initTotals()
       {
           return new SchemeType(ReportTotals::class);
       }
   
       public static function getLayer(): int
       {
           return 101;
       }
   
       protected function __construct($rows, $offset, $limit, $totalRows, $totals, $messages)
       {
           $this->rows = $rows;
           $this->offset = $offset;
           $this->limit = $limit;
           $this->totalRows = $totalRows;
           $this->totals = $totals;
           $this->messages = $messages;
       }
   
       public static function fromState(array $state): MappedScheme
       {
           return new self(
               self::restore('rows', $state),
               self::restore('offset', $state),
               self::restore('limit', $state),
               self::restore('totalRows', $state),
               self::restore('totals', $state),
               self::restore('messages', $state)
           );
       }
   }
```

Ответ API может содержать следующие строки, значит для них укажем публичные поля
 * rows
 * offset
 * limit
 * totalRows
 * totals
 * messages

Поле rows является массивом, т.е. коллекцией схем, которые тоже нужно мапить, указываем каким классом мапить
 
```       
public static function initRows()
{
   return new FieldBasedCollection(ReportRow::class);
}
```

Соответсвенно объявляем, что это поле у нас будет коллекцией 
```
public static function getMappedFields(): array
{
   return [
       'rows' => FieldBasedCollection::class
   ];
}
```

Указываем ID слоя, чем больше, тем глубже, т.е. ReportRows уже должен будет иметь на 1 больше(102)
```
public static function getLayer(): int
{
   return 101;
}
```
Аналогично мапятся остальные поля ответа
#!/usr/bin/env bash

args_string=''
for arg in "$@"
do
    args_string+=' '
    args_string+=${arg}
done

docker exec vol_frontend_php_1 composer ${args_string}
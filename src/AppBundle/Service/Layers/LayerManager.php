<?php

namespace AppBundle\Service\Layers;

use AppBundle\Scheme\Mapping\MappedSchemes;

class LayerManager
{
    private $schemes;
    private $layers;

    public function __construct(MappedSchemes $mappedSchemes)
    {
        $raw = [];
        $schemes = $mappedSchemes::get();
        foreach ($schemes as $id => $scheme) {
            $raw[] = [
                'id' => $id,
                'scheme' => $scheme,
                'layer' => (new \ReflectionMethod($scheme, 'getLayer'))->invoke(null),
            ];
        }

        $this->layers = \array_column($raw, 'layer', 'id');
        $this->schemes = \array_column($raw, 'scheme', 'id');
    }

    public function getLayers(): array
    {
        return $this->layers;
    }

    public function getLayer(string $scheme)
    {
        if ($id = \array_search($scheme, $this->schemes, true)) {
            return $this->layers[$id];
        }

    }

    public function getParent(string $scheme): ?string
    {
        if (false !== $id = array_search($this->getLayer($scheme) - 1, $this->layers, true)) {
            return $this->schemes[$id];
        }

        return null;
    }

    public function getChildren(string $scheme): ?array
    {
        $children = [];
        foreach ($this->layers as $id => $layer) {
            if ($layer === $this->getLayer($scheme) + 1) {
                $children[] = $this->schemes[$id];
            }
        }

        return $children;
    }
}
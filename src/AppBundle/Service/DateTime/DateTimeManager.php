<?php

namespace AppBundle\Service\DateTime;

class DateTimeManager
{
    public const DEFAULT_TIME_ZONE = 'Asia/Tomsk';

    private $voluumTimeZone = 'Etc/GMT';
    private $frontendTimeZone = null;

    private $timeZones = [
        '+3000' => 'Europe/Moscow',
        '+7000' => 'Asia/Tomsk',
    ];

    public function format(string $time = 'now', string $timezone = self::DEFAULT_TIME_ZONE): string
    {
        return (new \DateTime($time))
            ->setTimezone(new \DateTimeZone($timezone))
            ->format('Y-m-d\TH:00:00O');
    }

    /*
     * Зона, в которой волуум будет возвращать ответ
     */
    public function getVoluumTimeZone(): string
    {
        return $this->voluumTimeZone;
    }

    /*
     * Зона, в которой рендерим, должна совпадать с зоной, которую отдает клиент
     */
    public function getAppTimeZone(): string
    {
        if (null !== $this->frontendTimeZone) {
            return $this->frontendTimeZone;
        }

        return self::DEFAULT_TIME_ZONE;
    }

    /*
     * @TODO Изменить $_COOKIE на внутрейнний механизм работы с куками
     */
    public function setFrontendTimeZone(): void
    {
        $timezone = $_COOKIE['timezone'] ?? self::DEFAULT_TIME_ZONE;

        $this->frontendTimeZone = $timezone;
    }

//    private function getTimeZoneName(string $timezone): string
//    {
//        if (isset($this->timeZones[$timezone])) {
//            return $this->timeZones[$timezone];
//        }
//
//        return self::DEFAULT_TIME_ZONE;
//    }
}
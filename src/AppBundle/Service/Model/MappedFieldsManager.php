<?php

namespace AppBundle\Service\Model;

use AppBundle\Scheme\Types\CollectionType;
use AppBundle\Scheme\Types\SchemeType;
use AppBundle\Scheme\Types\Type;

/**
 * На каждую модель хранятся поля с их инциализированными типами
 * В процессе работы приложения тип не сразу получает данные, а при обрезке ненужных значений
 * @see StateService::class;
 *
 */
class MappedFieldsManager
{
    private $model;
    private $storage = [];

    /**
     * @param string $model Схема, в которой определены методы мапинга
     */
    public function setModel(string $model): void
    {
        $this->model = $model;
    }

    public function getCollection($field): CollectionType
    {
        $this->prepare();
        $method = sprintf('init%s', ucfirst($field));

        return $this->getType($method, $field);
    }


    public function getScheme($field): SchemeType
    {
        $this->prepare();
        $method = sprintf('init%s', ucfirst($field));

        return $this->getType($method, $field);
    }

    /**
     * @param string $methodName
     * @param string $field
     * @return Type|CollectionType|SchemeType
     */
    private function getType(string $methodName, string $field)
    {
        if (!isset($this->storage[$this->model][$field])) {
            /**
             * @var Type $initiatedType
             */
            $initiatedType = $this->model::{$methodName}();
            $this->storage[$this->model][$field] = $initiatedType;

            return $initiatedType;
        }

        return $this->storage[$this->model][$field];

    }

    private function prepare(): void
    {
        if (null === $this->model) {
            throw new \RuntimeException('Set model');
        }
    }

    public function addState($field, $state): void
    {
        if (!isset($this->storage[$this->model][$field])) {
            throw new \RuntimeException('Cant find mapped field');
        }

        $this->storage[$this->model][$field]->setState($state);
    }

    public function getState($field)
    {
        return $this->storage[$this->model][$field]->getState();
    }
}
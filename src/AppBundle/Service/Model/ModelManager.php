<?php

namespace AppBundle\Service\Model;

use AppBundle\Scheme\MappedModels;
use AppBundle\Scheme\Mapping\MappedSchemes;

class ModelManager
{
    private $models;

    public function __construct(MappedSchemes $schemes, MappedModels $models)
    {
        $this->models = $schemes::load(function () use ($models) {
            return $models::get();
        });
    }

    private function getScheme($id): ?string
    {
        return $this->models::getOne($id);
    }

    public function findModel($className): ?int
    {
        return $this->models::find($className);
    }

    public function getModel($id): string
    {
        $scheme = $this->getScheme($id);
        if (null === $scheme) {
            throw new \InvalidArgumentException('Scheme not exist');
        }

        return $scheme;
    }

    public function classExist($className): bool
    {
        return null !== $this->models::find($className);
    }

    /**
     * Пример кложура, принимает на вход модель
     * <pre>
     * function (string $model) {
     *     return $model::getVoluumName();
     * }
     * </pre>
     * @param \Closure $closure
     * @return array
     */
    public function getFromAll(\Closure $closure): array
    {
        $ids = \array_keys($this->models::get());

        $result = [];
        foreach ($ids as $id) {
            $result[$id] = $closure($this->getModel($id));
        }

        return $result;
    }
}
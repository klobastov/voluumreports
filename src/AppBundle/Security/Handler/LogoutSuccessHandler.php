<?php

namespace AppBundle\Security\Handler;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\Logout\DefaultLogoutSuccessHandler;

class LogoutSuccessHandler extends DefaultLogoutSuccessHandler
{
    public function onLogoutSuccess(Request $request)
    {
        if (null !== $session = $request->getSession()) {
            $session->remove('CWAUTH-TOKEN');
            $session->remove('secret');
        }

        return $this->httpUtils->createRedirectResponse($request, $this->targetUrl);
    }
}
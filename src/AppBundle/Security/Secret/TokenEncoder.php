<?php

namespace AppBundle\Security\Secret;

class TokenEncoder
{
    private $chipher = 'aes-128-cbc';

    public function encode(string $token, $secret): ?string
    {
        $iv = $this->getVector($secret);
        $phrase = openssl_encrypt($token, $this->chipher, $secret, 0, $iv);

        return false !== $phrase ? $phrase : null;
    }

    public function decode(string $phrase, $secret): ?string
    {
        $iv = $this->getVector($secret);
        $token = openssl_decrypt($phrase, $this->chipher, $secret, 0, $iv);

        return false !== $token ? $token : null;
    }

    private function getVector(string $secret): string
    {
        $pseudoRandomString = str_repeat($secret, 16);

        $step1 = substr($pseudoRandomString, \strlen($this->chipher));
        if (\strlen($step1) >= 16) {
            return substr($step1, 0, 15);
        }

        return substr(str_repeat($step1, 16), 0, 15);
    }
}
<?php

namespace AppBundle\Security\User;

use Symfony\Component\Security\Core\User\UserInterface;

class User implements UserInterface
{
    private $token;

    public function __construct(string $cwatoken)
    {
        $this->token = $cwatoken;
    }

    public function getToken(): string
    {
        return $this->token;
    }

    public function getRoles()
    {
        return ['ROLE_CWAT_USER'];
    }

    public function getUsername()
    {
        return 'rucoder';
    }

    public function getPassword()
    {
    }

    public function getSalt()
    {
        return null;
    }

    public function eraseCredentials()
    {
    }
}
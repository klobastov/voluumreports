<?php

namespace AppBundle\Security\UserProvider;

use AppBundle\Security\User\User;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class UserProvider implements UserProviderInterface
{
    public function loadUserByUsername($username)
    {
        $cwatoken = $username;

        if (null !== $cwatoken) {
            return new User($cwatoken);
        }

        throw new UsernameNotFoundException('Empty token.');
    }

    public function refreshUser(UserInterface $user)
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(
                sprintf('Instances of "%s" are not supported.', \get_class($user))
            );
        }

        return $this->loadUserByUsername($user->getToken());
    }

    public function supportsClass($class)
    {
        return User::class === $class;
    }
}
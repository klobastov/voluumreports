<?php

namespace AppBundle\Security\Voluum;

use HttpResponseException;
use Requests;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class CWAuthToken
{
    const ACCESS_KEY_ID = 'accessId';
    const ACCESS_KEY = 'accessKey';

    private function getAccessKeys(): array
    {
        return [
            [
                self::ACCESS_KEY_ID => 'c69bc909-72e3-4f62-8f6f-f6fb2904b2b0',
                self::ACCESS_KEY => '0inIQdn-Kb8PIo4eoHXQTrSrTx0KBWMH9CLA',
            ],
        ];
    }

    public function get(): string
    {
        $variantsOfKeyPair = $this->getAccessKeys();
        $randomKeyPair = $variantsOfKeyPair[array_rand($variantsOfKeyPair, 1)];
        $request = Requests::post('http://api.voluum.com/auth/access/session',
            [
                'Accept' => 'application/json; charset=utf-8',
                'Content-Type' => 'application/json; charset=utf-8',
            ],
            json_encode($randomKeyPair)
        );
        if (200 !== $request->status_code) {
            throw new BadRequestHttpException('Cant create voluum session, http code: '. $request->status_code);
        }

        $content = json_decode($request->body, true);
        if (!isset($content['token'])) {
            throw new HttpResponseException('Body does not contain "token" field');
        }

        return $content['token'];
    }

    public function isValid(string $token): bool
    {
        $request = Requests::get('http://api.voluum.com/auth/session',
            ['Accept' => 'application/json', 'CWAUTH-TOKEN' => $token]
        );

        if (200 !== $request->status_code) {
            throw new BadRequestHttpException('Cant check is valid token or not');
        }

        $content = json_decode($request->body, true);
        if (!isset($content['alive'])) {
            throw new HttpResponseException('Body does not contain "alive" field');
        }

        return $content['alive'];
    }
}
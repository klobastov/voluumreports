<?php

namespace AppBundle\Security\Authenticator;

use AppBundle\Security\Voluum\CWAuthToken;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;

class TokenAuthenticator extends AbstractGuardAuthenticator
{
    private $authToken;
    private $requests;
    private $secrets;

    public function __construct(CWAuthToken $authToken, RequestStack $requests, array $secrets)
    {
        $this->authToken = $authToken;
        $this->requests = $requests;
        $this->secrets = $secrets;
    }

    public function start(Request $request, AuthenticationException $authException = null)
    {
        $data = array(
            'message' => 'Authentication Required',
        );

        return new JsonResponse($data, Response::HTTP_UNAUTHORIZED);
    }

    public function getCredentials(Request $request)
    {
        return array(
            'secret' => $request->request->get('secret'),
        );
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $request = $this->requests->getCurrentRequest();

        //if secret not exist or bad should re authorisation
        $secret = $credentials['secret'];
        if (null === $secret && null !== $session = $request->getSession()) {
            $secret = $session->get('secret');
        }

        if (!\in_array($secret, array_column($this->secrets, 'secret'), false)) {
            if (null !== $secret && null !== $session = $request->getSession()) {
                $session->getFlashBag()->set('error', 'Authenticate failed');
            }

            return null;
        }

        //always should be token per request
        $cwatoken = null;
        if (null !== $session = $request->getSession()) {
            $cwatoken = $session->get('CWAUTH-TOKEN');
        }

        //@TODO!! добавить проверку сессии периодичную, а не при кадом запросе
//        if (null === $cwatoken || (null !== $cwatoken && !$this->authToken->isValid($cwatoken))) {
        if (null === $cwatoken) {
            $cwatoken = $this->authToken->get();
        }

        if ((null !== $cwatoken && null !== $session = $request->getSession()) &&
            $cwatoken !== $session->get('CWAUTH-TOKEN') &&
            $cwatoken !== $session->get('secret')
        ) {
            $session->set('CWAUTH-TOKEN', $cwatoken);
            $session->set('secret', $secret);
            $session->getFlashBag()->set('success', 'Authenticate successfully complete.');
        }

        return $userProvider->loadUserByUsername($cwatoken);
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        return true;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        return null;
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        return null;
    }

    public function supportsRememberMe()
    {
        return false;
    }
}
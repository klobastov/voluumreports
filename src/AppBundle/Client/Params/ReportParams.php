<?php

namespace AppBundle\Client\Params;

class ReportParams extends VoluumParams
{
    /*
     * Зона в которой придёт ответ
     */
    private $tz;
    private $for;
    private $from;
    private $to;
    private $limit;
    private $offset;
    private $groupBy;
    private $filter;
    private $columns;
    private $filters;
    private $page;

    public function __construct(
        string $tz,
        string $for,
        string $from,
        string $to,
        int $limit,
        int $offset,
        array $groupBy,
        string $filter,
        array $columns,
        array $filters,
        $page
    ) {
        $this->tz = $tz;
        $this->for = $for;
        $this->from = $from;
        $this->to = $to;
        $this->limit = $limit;
        $this->offset = $offset;
        $this->groupBy = $groupBy;
        $this->filter = $filter;
        $this->columns = $columns;
        $this->filters = $filters;
        $this->page = $page;
    }

    public function getPage()
    {
        return $this->page;
    }

    public function isValid()
    {
        try {
            $this->getRequiredParameters();
        } catch (\InvalidArgumentException $exception) {
            return false;
        }

        return true;
    }

    public function getVoluumQueryData(): array
    {
        $required = $this->getRequiredParameters();
        $other = $this->getResourceParameters();

        $queryData = array_merge_recursive($required, $other);
        $queryData = array_merge($queryData, $this->filters);
        ksort($queryData);
        $arrayParameters = $this->getArrayParameters();
        foreach ($queryData as $parameter => $value) {
            if (\is_array($value) && !\in_array($parameter, $arrayParameters, true)) {
                $queryData[$parameter] = array_shift($value);
            }
            if (null === $value || (!is_numeric($value) && empty($value))) {
                unset($queryData[$parameter]);
            }
        }

        return $queryData;
    }

    private function getArrayParameters()
    {
        $arrayParameters = [];
        foreach ($parameters = $this->getResourceParameters() as $parameter => $value) {
            if (\is_array($value)) {
                $arrayParameters[] = $parameter;
            }
        }

        return $arrayParameters;
    }

    private function getResourceParameters()
    {
        return [
            'from' => $this->from,
            'to' => $this->to,
            'filter' => $this->filter,
            'offset' => $this->offset,
            'tz' => $this->tz,
            'columns' => $this->columns, //array
            'limit' => $this->limit,
            'groupBy' => $this->groupBy, //array
        ];
    }

    private function getRequiredParameters()
    {
        if (null === $this->from || null === $this->for) {
            throw new \InvalidArgumentException('Required parameters is empty');
        }

        return [
            'from' => $this->from,
            'groupBy' => [0 => $this->for], //array, first = report page in groupBy context
        ];
    }

    public function getFor()
    {
        return $this->for;
    }

    public function getGroupBy()
    {
        return $this->groupBy;
    }

    public function getFilters()
    {
        $filters = [];

        $iFilter = 1;
        foreach ($this->filters as $row) {
            if (isset($this->filters["filter{$iFilter}"], $this->filters["filter{$iFilter}Value"])) {
                $filters[$this->filters["filter{$iFilter}"]] = $this->filters["filter{$iFilter}Value"];
                $iFilter++;
            }
        }

        return array_reverse($filters);
    }
}
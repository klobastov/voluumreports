<?php

namespace AppBundle\Client\Params;

use AppBundle\Service\DateTime\DateTimeManager;
use Symfony\Component\HttpFoundation\Request;

/**
 * @todo отрефакторить на раззные классы сложные параметры
 */
class ParamsFactory
{
    protected const  QUERY_REPORT_NAME = 'for';

    protected const QUERY_PAGE_LIMIT = 'limit';
    protected const QUERY_PAGE = 'page';
    protected const QUERY_GROUP_BY = 'groupBy';
    protected const QUERY_COLUMNS_FOR_TEXT_FILTER = 'columns';
    protected const QUERY_SEARCH_BY_COLUMNS = 'filter';
    protected const QUERY_DATETIME_FROM_FILTER = 'from';
    protected const QUERY_DATETIME_TO_FILTER = 'to';
    protected const QUERY_MODEL = 'model';
    protected const QUERY_MODEL2 = 'model2';
    protected const QUERY_IDENTIFIER = 'identifier';
    protected const QUERY_IDENTIFIER2 = 'identifier2';
    protected const QUERY_OPTIONS_FILTER = 'opt';
    protected const QUERY_OPTION_FILTER = 'nm';
    protected const QUERY_OPTION_VALUE = 'vl';
    protected const QUERY_OPTION_RELATION = 'rl';
    protected const QUERY_OPTION_VALUE_MORE = 'm';
    protected const QUERY_OPTION_VALUE_EQUALS = 'e';
    protected const QUERY_OPTION_VALUE_LESS = 'l';

    private $timeManager;

    private $filters = [];
    private $filtersIterator = 1;

    public function __construct(DateTimeManager $timeManager)
    {
        $this->timeManager = $timeManager;
    }

    public function createFromRequest(Request $request): ReportParams
    {
        $for = $request->query->get(self::QUERY_REPORT_NAME, 'campaign');
        $limit = $request->query->getInt(self::QUERY_PAGE_LIMIT, 30);
        $page = $request->query->getInt(self::QUERY_PAGE, 1);

        $offset = ($page * $limit) - $limit;

        $groupBy = $request->query->get(self::QUERY_GROUP_BY, []);

        $columns = $request->query->get(self::QUERY_COLUMNS_FOR_TEXT_FILTER, []);
        $filter = $request->query->get(self::QUERY_SEARCH_BY_COLUMNS, '');

        $tzApp = $this->timeManager->getAppTimeZone();
        $fromTz = $this->timeManager->format($request->query->get(self::QUERY_DATETIME_FROM_FILTER,
            $this->timeManager->format('-1 week', $tzApp)
        ), $tzApp);
        $toTz = $this->timeManager->format($request->query->get(self::QUERY_DATETIME_TO_FILTER,
            $this->timeManager->format('now', $tzApp)
        ), $tzApp);

        $model = $request->query->get(self::QUERY_MODEL, '');
        $identifier = $request->query->get(self::QUERY_IDENTIFIER, '');

        $model2 = $request->query->get(self::QUERY_MODEL2, '');
        $identifier2 = $request->query->get(self::QUERY_IDENTIFIER2, '');

        $this->addFilter($model, $identifier);
        $this->addFilter($model2, $identifier2);

        $options = $request->query->get(self::QUERY_OPTIONS_FILTER, []);

        //Авто-фильтры
        //Скрытие кампаний, которые не активные(включает заархивированные)
        if ($for === 'campaign') {
            $this->addFilter('status', 'ACTIVE');
        }
        //Скрытие строк во всех отчётах, у которых нет визитов
        $this->addFilter('visits', '0', function (array &$filters, int $iterator) {
            $filters["filter{$iterator}Relation"] = $this->getOptionRelation(self::QUERY_OPTION_VALUE_MORE);
        });
        $this->aggregateOptionsFilter($options);

        return new ReportParams($this->timeManager->getVoluumTimeZone(),
            $for,
            $fromTz,
            $toTz,
            $limit,
            $offset,
            $groupBy,
            $filter,
            $columns,
            $this->filters,
            $page
        );
    }

    private function addFilter($filter, $value, \Closure $closure = null): void
    {
        if (empty($filter) && empty($value)) {
            return;
        }

        $iterator = $this->filtersIterator++;

        $this->filters["filter{$iterator}"] = $filter;
        $this->filters["filter{$iterator}Value"] = $value;

        if (\is_callable($closure)) {
            $closure($this->filters, $iterator);
        }
    }

    private function aggregateOptionsFilter(array $options): void
    {
        foreach ($options as $option) {
            $this->addFilter(
                $this->getOptionValue(self::QUERY_OPTION_FILTER, $option),
                $this->getOptionValue(self::QUERY_OPTION_VALUE, $option),
                function (array &$filters, int $iterator) use ($option) {
                    $filters["filter{$iterator}Relation"] =
                        $this->getOptionRelation(
                            $this->getOptionValue(self::QUERY_OPTION_RELATION, $option)
                        );
                }
            );
        }
    }

    private function getOptionRelation(string $alias): ?string
    {
        switch ($alias) {
            case self::QUERY_OPTION_VALUE_EQUALS:
                return 'EQUAL';
            case self::QUERY_OPTION_VALUE_LESS:
                return 'LESS';
            case self::QUERY_OPTION_VALUE_MORE:
                return 'GREATER';
        }
    }

    private function getOptionValue(string $name, array $option): ?string
    {
        if (!isset($option[$name])) {
            return null;
        }

        return $option[$name];
    }
}
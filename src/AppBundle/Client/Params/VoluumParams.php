<?php

namespace AppBundle\Client\Params;

abstract class VoluumParams
{
    abstract public function getVoluumQueryData(): array;
}
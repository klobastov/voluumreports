<?php

namespace AppBundle\Client\Request;

use AppBundle\Client\Params\VoluumParams;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class RequestFactory
{
    private $domain;
    private $token;

    public function __construct(TokenStorage $tokenStorage)
    {
        $this->domain = 'http://api.voluum.com';

        $user = null;
        if (null !== $token = $tokenStorage->getToken()) {
            $user = $token->getUser();
        }

        if (null !== $user) {
            $this->token = $user->getToken();
        }
    }

    public function createFromParams(string $resource, VoluumParams $params): VoluumRequest
    {
        $url = $this->domain.'/'.$resource.'?'.http_build_query($params->getVoluumQueryData());
        $hotFixForVoluumArrayVars = preg_replace('#(\%5B\d\%5D)#', '', $url);

        if (null === $this->token) {
            throw new \RuntimeException('Cant get voluum token');
        }

        return new VoluumRequest($hotFixForVoluumArrayVars, $this->token);
    }
}
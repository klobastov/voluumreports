<?php

namespace AppBundle\Client\Request;

use AppBundle\Client\Params\VoluumParams;

abstract class Service
{
    protected $implementation;

    abstract public function get(): array;

    public function __construct(VoluumParams $params)
    {
        $this->implementation = $params;
    }

    public function setImplementation(VoluumParams $params): void
    {
        $this->implementation = $params;
    }
}
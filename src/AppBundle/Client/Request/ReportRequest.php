<?php

namespace AppBundle\Client\Request;

use AppBundle\Client\Params\ReportParams;

class ReportRequest extends VoluumRequest
{
    private $params;

    public function __construct($url, $token, ReportParams $params)
    {
        parent::__construct($url, $token);

        $this->params = $params;
    }

    public function getFor()
    {
        return $this->params->getFor();
    }

    public function getGroupBy()
    {
        return $this->params->getGroupBy();
    }
}
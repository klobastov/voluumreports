<?php

namespace AppBundle\Client\Request;

use Requests;
use Requests_Response;

class VoluumRequest
{
    private $url;
    private $response;
    private $token;

    public function __construct(string $url, string $token)
    {
        $this->url = $url;
        $this->token = $token;
    }

    public function send(): void
    {
        $this->response = Requests::get($this->url, [
            'Accept' => 'application/json; charset=utf-8',
            'Content-Type' => 'application/x-www-form-urlencoded',
            'CWAUTH-TOKEN' => $this->token,
        ],['connect_timeout'=>30,'timeout'=>30]);

    }

    public function getResponse(): Requests_Response
    {
        return $this->response;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function getToken(): string
    {
        return $this->token;
    }
}
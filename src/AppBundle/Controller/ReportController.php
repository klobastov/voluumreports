<?php

namespace AppBundle\Controller;

use AppBundle\Client\Params\ParamsFactory;
use AppBundle\Client\Request\RequestFactory;
use AppBundle\Scheme\Mapping\DataMapper\Factory\ReportFactory;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ReportController extends Controller
{
    private $requestFactory;
    private $reportFactory;
    private $paramsFactory;

    public function __construct(
        ParamsFactory $paramsFactory,
        RequestFactory $requestFactory,
        ReportFactory $reportFactory
    ) {
        $this->paramsFactory = $paramsFactory;
        $this->requestFactory = $requestFactory;
        $this->reportFactory = $reportFactory;
    }

    public function defaultAction(Request $request)
    {
        $params = $this->paramsFactory->createFromRequest($request);
        if (!$params->isValid()) {
            $this->addFlash('warning', 'Request params is invalid');

            $this->redirectToRoute('homepage');
        }

        $voluumRequest = $this->requestFactory->createFromParams('report', $params);
        $report = $this->reportFactory->createFromRequest($voluumRequest);

        return $this->render('report/resolve.html.twig', [
            'report' => $report,
            'params' => $params,
            'page' => $params->getPage(),
        ]);
    }
}
<?php

namespace AppBundle\Twig;

use AppBundle\Model\Error;
use AppBundle\Model\Report;
use AppBundle\Service\Model\MappedFieldsManager;

class ReportExtension extends \Twig_Extension
{
    /**
     * @var MappedFieldsManager
     */
    private $mappedFieldsManager;

    public function __construct(MappedFieldsManager $mappedFieldsManager)
    {
        $this->mappedFieldsManager = $mappedFieldsManager;
        $this->mappedFieldsManager->setModel(Report::class);
    }

    public function getFunctions(): array
    {
        return [
            new \Twig_SimpleFunction('getReportTemplate', [$this, 'resolve']),
            new \Twig_SimpleFunction('is_numeric', [$this, 'isNumeric']),
        ];
    }

    public function resolve($report)
    {
        if ($report instanceof Report) {
            if (empty($this->mappedFieldsManager->getState('rows'))) {
                return 'report/reports/empty.html.twig';
            }

            return 'report/reports/default.html.twig';
        }

        if ($report instanceof Error) {
            return 'report/error.html.twig';
        }

        return 'errors/reportUnknown.html.twig';
    }
    
    public function isNumeric($value) 
    {
        return is_numeric($value);    
    }
}
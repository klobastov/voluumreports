<?php

namespace AppBundle\Twig;

use AppBundle\Service\Model\ModelManager;
use Twig_Extension;
use Twig_SimpleFunction;

/*
 * Расширения для сайдбара, чтобы в ссылках имспользовать данные их схем
 */
class VoluumNameExtension extends Twig_Extension
{
    private $modelManager;

    public function __construct(ModelManager $modelManager)
    {
        $this->modelManager = $modelManager;
    }

    public function getFunctions(): array
    {
        return [
            new Twig_SimpleFunction('getVoluumName', [$this, 'resolve']),
        ];
    }

    public function resolve(int $id)
    {
        if (null !== $scheme = $this->modelManager->getModel($id)) {
            return $scheme::getVoluumName();
        }

        throw new \InvalidArgumentException('Unknown model');
    }
}
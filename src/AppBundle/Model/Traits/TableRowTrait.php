<?php

namespace AppBundle\Model\Traits;

trait TableRowTrait
{
    abstract public static function getColumnsOrder(): array;

    abstract public static function getExcludeColumns(): array;

    abstract public static function getNameColumns(): array;

    abstract public static function getIdentifier(): ?string;

    public static function getFilledFields(): array
    {
        return [];
    }

    /*
     * Получисть список столбцов с учетом переименования и смены позиции в таблице
     */
    public static function getColumns(): array
    {
        $allowed = array_diff(static::getFilledFields(), static::getExcludeColumns());
        $columns = array_merge(array_flip($allowed), static::getNameColumns());

        array_walk($columns, function (&$value, $key) {
            if (!\is_string($value)) {
                $value = $key;
            }
        });

        $sorted = [];
        $order = self::getColumnsOrder();
        foreach ($order as $column) {
            if (isset($columns[$column])) {
                $sorted[$column] = $columns[$column];
            }
        }

        return \array_merge($sorted, $columns);
    }

    public function hasIdentifier(): bool
    {
        return null !== $this->getIdentifier();
    }

    public function getSecondIdentifier(): ?string
    {
        return null;
    }

    public function hasSecondIdentifier(): bool
    {
        return null !== $this->getSecondIdentifier();
    }
}
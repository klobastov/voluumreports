<?php

namespace AppBundle\Model\Traits;

trait VoluumReportTrait
{
    abstract public static function getSchemeName(): string;

    abstract public static function getVoluumName(): string;

    abstract public function getVoluumNames(): array;

    abstract public function getSchemeNames(): array;

    abstract public function getDisabledGroups(): array;

    public function getVoluumGroups(): array
    {
        $all = array_combine($this->getVoluumNames(), $this->getSchemeNames());
        $disabled = $this->getDisabledGroups();

        return array_diff($all, $disabled);
    }
}
<?php

namespace AppBundle\Model;

class ModelRow extends ReportRow
{
    public $model;

    public static function getExcludeColumns(): array
    {
        return array_merge(parent::getExcludeColumns(), []);
    }

    public static function getNameColumns(): array
    {
        return array_merge(parent::getNameColumns(), []);
    }

    public static function getIdentifier(): ?string
    {
        return 'model';
    }

    public static function getVoluumName(): string
    {
        return 'model';
    }

    public static function getSchemeName(): string
    {
        return 'model';
    }

    public function getDisabledGroups(): array
    {
        return [
            self::$modelManager->getModel(12)::getSchemeName(),
        ];
    }
}
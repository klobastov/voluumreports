<?php

namespace AppBundle\Model;

class ReferrerdomainRow extends ReportRow
{
    public $referrerDomain;

    public static function getExcludeColumns(): array
    {
        return array_merge(parent::getExcludeColumns(), []);
    }

    public static function getNameColumns(): array
    {
        return array_merge(parent::getNameColumns(), [
            'referrerDomain' => self::getSchemeName(),
        ]);
    }

    public static function getIdentifier(): ?string
    {
        return 'referrerDomain';
    }

    public static function getVoluumName(): string
    {
        return 'referrer-domain';
    }

    public static function getSchemeName(): string
    {
        return 'referer domain';
    }

    public function getDisabledGroups(): array
    {
        return [
            self::$modelManager->getModel(13)::getSchemeName(),
        ];
    }
}
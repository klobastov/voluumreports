<?php

namespace AppBundle\Model;

class DynamicRow extends ReportRow
{
    protected static $groups;
    protected static $fields = [];

    public static function getFilledFields(): array
    {
        return self::$fields;
    }

    public function __construct($state, array $fields, array $groups)
    {
        self::$modelManager = self::restore('modelManager', $state);
        self::$groups = $groups;
        self::$fields = $fields;

        foreach ($fields as $field) {
            if ($field !== 'modelManager') {
                $this->{$field} = self::restore($field, $state);
            }
        }

        parent::__construct(
            self::restore('modelManager', $state),
            self::restore('visits', $state),
            self::restore('clicks', $state),
            self::restore('conversions', $state),
            self::restore('revenue', $state),
            self::restore('cost', $state),
            self::restore('profit', $state),
            self::restore('cpv', $state),
            self::restore('ctr', $state),
            self::restore('cr', $state),
            self::restore('cv', $state),
            self::restore('roi', $state),
            self::restore('ecpa', $state)
        );
    }

    public static function getExcludeColumns(): array
    {
        $exclude = [];
        foreach (self::$groups as $group) {
            $className = self::getModelClassByVoluumName($group);
            $id = self::$modelManager->findModel($className);
            $exclude = \array_merge(self::$modelManager->getModel($id)::getExcludeColumns(), $exclude);
        }

        return array_merge(parent::getExcludeColumns(), $exclude);
    }

    public static function getNameColumns(): array
    {
        $names = [];
        foreach (self::$groups as $group) {
            $className = self::getModelClassByVoluumName($group);
            $id = self::$modelManager->findModel($className);
            $names = \array_merge(self::$modelManager->getModel($id)::getNameColumns(), $names);
        }

        return array_merge(parent::getNameColumns(), $names);
    }

    public static function getIdentifier(): ?string
    {
        if (\count(self::$groups) < 1) {
            return null;
        }

        $className = self::getModelClassByVoluumName(self::$groups[0]);
        $id = self::$modelManager->findModel($className);

        return self::$modelManager->getModel($id)::getIdentifier();
    }

    public function getSecondIdentifier(): ?string
    {
        if (\count(self::$groups) <= 1) {
            return null;
        }

        $className = self::getModelClassByVoluumName(self::$groups[1]);
        $id = self::$modelManager->findModel($className);

        return self::$modelManager->getModel($id)::getIdentifier();
    }

    public function getDisabledGroups(): array
    {
        $disabled = [];
        foreach (self::$groups as $group) {
            $className = self::getModelClassByVoluumName($group);
            $id = self::$modelManager->findModel($className);
            $disabled [] = self::$modelManager->getModel($id)::getSchemeName();
        }

        return $disabled;
    }

    private static function getModelClassByVoluumName($voluumName)
    {
        //Подготовки к внутренниму виду назвнаий моделей в проекте
        $name = \ucfirst($voluumName).'Row';
        $name = \str_replace('-', '', $name);

        $fileName = 'AppBundle\\Model\\'.$name;
        if (self::$modelManager->classExist($fileName)) {
            return $fileName;
        }

        throw new \RuntimeException('undefined class');
    }
}
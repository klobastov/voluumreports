<?php

namespace AppBundle\Model;

class OfferRow extends ReportRow
{
    public $offerId;
    public $offerName;

    public static function getExcludeColumns(): array
    {
        return array_merge(parent::getExcludeColumns(), ['offerId']);
    }

    public static function getNameColumns(): array
    {
        return array_merge(parent::getNameColumns(), [
            'offerName' => self::getSchemeName(),
        ]);
    }

    public static function getIdentifier(): ?string
    {
        return 'offerId';
    }

    public static function getVoluumName(): string
    {
        return 'offer';
    }

    public static function getSchemeName(): string
    {
        return 'offer';
    }

    public function getDisabledGroups(): array
    {
        return [
            self::$modelManager->getModel(2)::getSchemeName(),
        ];
    }
}
<?php

namespace AppBundle\Model;

use AppBundle\Scheme\Mapping\MappedScheme;
use AppBundle\Scheme\Nesting\LayerNestingScheme;

class ReportMessage extends LayerNestingScheme
{
    public $level;
    public $code;
    public $message;

    public static function getMappedFields(): array
    {
        return [];
    }

    protected function __construct($level, $code, $message)
    {
        $this->level = $level;
        $this->code = $code;
        $this->message = $message;
    }


    public static function getLayer(): int
    {
        return 102;
    }

    public static function fromState(array $state): MappedScheme
    {
        return new self(
            self::restore('level', $state),
            self::restore('code', $state),
            self::restore('message', $state)
        );
    }
}
<?php

namespace AppBundle\Model;

class BrowserRow extends ReportRow
{
    public $browser;

    public static function getExcludeColumns(): array
    {
        return array_merge(parent::getExcludeColumns(), []);
    }

    public static function getNameColumns(): array
    {
        return array_merge(parent::getNameColumns(), []);
    }

    public static function getIdentifier(): ?string
    {
        return 'browser';
    }

    public static function getVoluumName(): string
    {
        return 'browser';
    }

    public static function getSchemeName(): string
    {
        return 'browser';
    }

    public function getDisabledGroups(): array
    {
        return [
            self::$modelManager->getModel(6)::getSchemeName(),
        ];
    }
}
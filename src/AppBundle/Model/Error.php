<?php

namespace AppBundle\Model;


use AppBundle\Scheme\Mapping\MappedScheme;
use AppBundle\Scheme\Nesting\LayerNestingScheme;
use AppBundle\Scheme\Types\SchemeType;

class Error extends LayerNestingScheme
{
    public $error;
    public $reportRequest;
    public $voluumToken;

    public static function getMappedFields(): array
    {
        return [
            'error' => SchemeType::class,
        ];
    }

    public function initError()
    {
        return new SchemeType(ErrorMessage::class);
    }

    protected function __construct($error, $reportRequest, $voluumToken)
    {
        $this->error = $error;
        $this->reportRequest = $reportRequest;
        $this->voluumToken = $voluumToken;
    }

    public static function getLayer(): int
    {
        return 9000;
    }

    public static function fromState(array $state): MappedScheme
    {
        return new self(
            self::restore('error', $state),
            self::restore('reportRequest', $state),
            self::restore('voluumToken', $state)
        );
    }
}
<?php

namespace AppBundle\Model;

use AppBundle\Model\Traits\TableRowTrait;
use AppBundle\Model\Traits\VoluumReportTrait;
use AppBundle\Scheme\Mapping\MappedScheme;
use AppBundle\Scheme\Nesting\LayerNestingScheme;
use AppBundle\Service\Model\ModelManager;

class ReportRow extends LayerNestingScheme
{
    use TableRowTrait, VoluumReportTrait;

    /**
     * @var ModelManager
     */
    public static $modelManager;

    public $visits;
    public $clicks;
    public $conversions;
    public $revenue;
    public $cost;
    public $profit;
    public $cpv;
    public $ctr;
    public $cr;
    public $cv;
    public $roi;
    public $ecpa;

    private $voluumNames;
    private $schemeNames;

    public static function fromState(array $state): MappedScheme
    {
        return new self(
            self::restore('modelManager', $state),
            self::restore('visits', $state),
            self::restore('clicks', $state),
            self::restore('conversions', $state),
            self::restore('revenue', $state),
            self::restore('cost', $state),
            self::restore('profit', $state),
            self::restore('cpv', $state),
            self::restore('ctr', $state),
            self::restore('cr', $state),
            self::restore('cv', $state),
            self::restore('roi', $state),
            self::restore('ecpa', $state)
        );
    }

    public static function getMappedFields(): array
    {
        return [];
    }

    public static function getLayer(): int
    {
        return 102;
    }

    public static function getVoluumName(): string
    {
        return '';
    }

    public static function getSchemeName(): string
    {
        return 'default';
    }

    protected function __construct(
        $modelManager,
        $visits,
        $clicks,
        $conversions,
        $revenue,
        $cost,
        $profit,
        $cpv,
        $ctr,
        $cr,
        $cv,
        $roi,
        $ecpa
    ) {

        self::$modelManager = $modelManager;

        $this->visits = $visits;
        $this->clicks = $clicks;
        $this->conversions = $conversions;
        $this->revenue = $revenue;
        $this->cost = $cost;
        $this->profit = $profit;
        $this->cpv = $cpv;
        $this->ctr = $ctr;
        $this->cr = $cr;
        $this->cv = $cv;
        $this->roi = $roi;
        $this->ecpa = $ecpa;

        //@todo вызов происходит на каждую строчку, нужно избавиться от этого
        $this->voluumNames = \array_unique(self::$modelManager->getFromAll(function (string $model) {
            return $model::getVoluumName();
        }));

        $this->schemeNames = \array_unique(self::$modelManager->getFromAll(function (string $model) {
            return $model::getSchemeName();
        }));
    }

    public function getVoluumNames(): array
    {
        return $this->voluumNames;
    }

    public function getSchemeNames(): array
    {
        return $this->schemeNames;
    }

    public function getDisabledGroups(): array
    {
        return $this->schemeNames;
    }

    public static function getExcludeColumns(): array
    {
        return ['modelManager'];
    }

    public static function getNameColumns(): array
    {
        return [];
    }

    public static function getIdentifier(): ?string
    {
        return null;
    }

    public static function getColumnsOrder(): array
    {
        return [
            'campaignName',
            'offerName',
            'landerName',
            'countryCode',
            'countryName',
            'os',
            'osVersion',
            'brand',
            'device',
            'model',
            'browser',
            'browserVersion',
            'deviceName',
            'mobileCarrier',
            'referrerDomain',
            'trafficSourceName',
        ];
    }
}
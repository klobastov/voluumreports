<?php

namespace AppBundle\Model;

class CampaignRow extends ReportRow
{
    public $campaignId;
    public $campaignName;

    public static function getExcludeColumns(): array
    {
        return array_merge(parent::getExcludeColumns(), [
            'campaignId',
        ]);
    }

    public static function getNameColumns(): array
    {
        return array_merge(parent::getNameColumns(), [
            'campaignName' => self::getSchemeName(),
        ]);
    }

    public static function getIdentifier(): ?string
    {
        return 'campaignId';
    }

    public static function getVoluumName(): string
    {
        return 'campaign';
    }

    public static function getSchemeName(): string
    {
        return 'campaign';
    }

    public function getDisabledGroups(): array
    {
        return [
            self::$modelManager->getModel(1)::getSchemeName(),
        ];
    }
}
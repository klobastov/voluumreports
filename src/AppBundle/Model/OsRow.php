<?php

namespace AppBundle\Model;

class OsRow extends ReportRow
{
    public $os;

    public static function getExcludeColumns(): array
    {
        return array_merge(parent::getExcludeColumns(), []);
    }

    public static function getNameColumns(): array
    {
        return array_merge(parent::getNameColumns(), []);
    }

    public static function getIdentifier(): ?string
    {
        return 'os';
    }

    public static function getVoluumName(): string
    {
        return 'os';
    }

    public static function getSchemeName(): string
    {
        return 'os';
    }

    public function getDisabledGroups(): array
    {
        return [
            self::$modelManager->getModel(8)::getSchemeName(),
        ];
    }
}
<?php

namespace AppBundle\Model;

class DeviceRow extends ReportRow
{
    public $device;

    public static function getExcludeColumns(): array
    {
        return array_merge(parent::getExcludeColumns(), []);
    }

    public static function getNameColumns(): array
    {
        return array_merge(parent::getNameColumns(), [
        ]);
    }

    public static function getIdentifier(): ?string
    {
        return 'device';
    }

    public static function getVoluumName(): string
    {
        return 'device';
    }

    public static function getSchemeName(): string
    {
        return 'device';
    }

    public function getDisabledGroups(): array
    {
        return [
            self::$modelManager->getModel(10)::getSchemeName(),
        ];
    }
}
<?php

namespace AppBundle\Model;

class MobilecarrierRow extends ReportRow
{
    public $mobileCarrier;

    public static function getExcludeColumns(): array
    {
        return array_merge(parent::getExcludeColumns(), []);
    }

    public static function getNameColumns(): array
    {
        return array_merge(parent::getNameColumns(), [
            'mobileCarrier' => self::getSchemeName(),
        ]);
    }

    public static function getIdentifier(): ?string
    {
        return 'mobileCarrier';
    }

    public static function getVoluumName(): string
    {
        return 'mobile-carrier';
    }

    public static function getSchemeName(): string
    {
        return 'carrier';
    }

    public function getDisabledGroups(): array
    {
        return [
            self::$modelManager->getModel(5)::getSchemeName(),
        ];
    }
}
<?php

namespace AppBundle\Model;

use AppBundle\Scheme\Mapping\MappedScheme;
use AppBundle\Scheme\Nesting\LayerNestingScheme;

class ErrorMessage extends LayerNestingScheme
{
    public $code;
    public $description;
    public $time;
    public $messages;

    public static function getMappedFields(): array
    {
        return [];
    }

    protected function __construct($code, $description, $time, $messages)
    {
        $this->code = $code;
        $this->description = $description;
        $this->time = $time;
        $this->messages = $messages;
    }

    public static function getLayer(): int
    {
        return 9001;
    }

    public static function fromState(array $state): MappedScheme
    {
        return new self(
            self::restore('code', $state),
            self::restore('description', $state),
            self::restore('time', $state),
            self::restore('messages', $state)
        );
    }

    public function getMessages()
    {
        return $this->messages;
    }
}
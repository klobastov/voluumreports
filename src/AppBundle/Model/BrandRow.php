<?php

namespace AppBundle\Model;

class BrandRow extends ReportRow
{
    public $brand;

    public static function getExcludeColumns(): array
    {
        return array_merge(parent::getExcludeColumns(), []);
    }

    public static function getNameColumns(): array
    {
        return array_merge(parent::getNameColumns(), []);
    }

    public static function getIdentifier(): ?string
    {
        return 'brand';
    }

    public static function getVoluumName(): string
    {
        return 'brand';
    }

    public static function getSchemeName(): string
    {
        return 'brand';
    }

    public function getDisabledGroups(): array
    {
        return [
            self::$modelManager->getModel(11)::getSchemeName(),
        ];
    }
}
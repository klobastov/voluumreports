<?php

namespace AppBundle\Model;

class CountrycodeRow extends ReportRow
{
    public $countryCode;
    public $countryName;

    public static function getExcludeColumns(): array
    {
        return array_merge(parent::getExcludeColumns(), ['countryCode']);
    }

    public static function getNameColumns(): array
    {
        return array_merge(parent::getNameColumns(), [
            'countryName' => self::getSchemeName(),
        ]);
    }

    public static function getIdentifier(): ?string
    {
        return 'countryCode';
    }

    public static function getVoluumName(): string
    {
        return 'country-code';
    }

    public static function getSchemeName(): string
    {
        return 'country';
    }

    public function getDisabledGroups(): array
    {
        return [
            self::$modelManager->getModel(4)::getSchemeName(),
        ];
    }
}
<?php

namespace AppBundle\Model;

use AppBundle\Scheme\Mapping\MappedScheme;
use AppBundle\Scheme\Nesting\LayerNestingScheme;

class ReportTotals extends LayerNestingScheme
{
    public $visits;
    public $clicks;
    public $conversions;
    public $revenue;
    public $cost;
    public $profit;
    public $cpv;
    public $ctr;
    public $cr;
    public $cv;
    public $roi;
    public $ecpa;

    public static function getMappedFields(): array
    {
        return [];
    }

    public function __construct(
        $visits,
        $clicks,
        $conversions,
        $revenue,
        $cost,
        $profit,
        $cpv,
        $ctr,
        $cr,
        $cv,
        $roi,
        $ecpa
    ) {

        $this->visits = $visits;
        $this->clicks = $clicks;
        $this->conversions = $conversions;
        $this->revenue = $revenue;
        $this->cost = $cost;
        $this->profit = $profit;
        $this->cpv = $cpv;
        $this->ctr = $ctr;
        $this->cr = $cr;
        $this->cv = $cv;
        $this->roi = $roi;
        $this->ecpa = $ecpa;
    }


    public static function getLayer(): int
    {
        return 102;
    }

    public static function fromState(array $state): MappedScheme
    {
        return new self(
            self::restore('visits', $state),
            self::restore('clicks', $state),
            self::restore('conversions', $state),
            self::restore('revenue', $state),
            self::restore('cost', $state),
            self::restore('profit', $state),
            self::restore('cpv', $state),
            self::restore('ctr', $state),
            self::restore('cr', $state),
            self::restore('cv', $state),
            self::restore('roi', $state),
            self::restore('ecpa', $state)
        );
    }
}
<?php

namespace AppBundle\Model;

class OsversionRow extends ReportRow
{
    public $osVersion;

    public static function getExcludeColumns(): array
    {
        return array_merge(parent::getExcludeColumns(), []);
    }

    public static function getNameColumns(): array
    {
        return array_merge(parent::getNameColumns(), [
            'osVersion' => self::getSchemeName(),
        ]);
    }

    public static function getIdentifier(): ?string
    {
        return 'osVersion';
    }

    public static function getVoluumName(): string
    {
        return 'os-version';
    }

    public static function getSchemeName(): string
    {
        return 'os version';
    }

    public function getDisabledGroups(): array
    {
        return [
            self::$modelManager->getModel(9)::getSchemeName(),
        ];
    }
}
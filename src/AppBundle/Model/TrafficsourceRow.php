<?php

namespace AppBundle\Model;

class TrafficsourceRow extends ReportRow
{
    public $trafficSourceId;
    public $trafficSourceName;

    public static function getExcludeColumns(): array
    {
        return array_merge(parent::getExcludeColumns(), [
            'trafficSourceId',
        ]);
    }

    public static function getNameColumns(): array
    {
        return array_merge(parent::getNameColumns(), [
            'trafficSourceName' => self::getSchemeName(),
        ]);
    }

    public static function getIdentifier(): ?string
    {
        return 'trafficSourceId';
    }

    public static function getVoluumName(): string
    {
        return 'traffic-source';
    }

    public static function getSchemeName(): string
    {
        return 'traffic source';
    }

    public function getDisabledGroups(): array
    {
        return [
            self::$modelManager->getModel(14)::getSchemeName(),
        ];
    }
}
<?php

namespace AppBundle\Model;

class LanderRow extends ReportRow
{
    public $landerId;
    public $landerName;

    public static function getExcludeColumns(): array
    {
        return array_merge(parent::getExcludeColumns(), [
            'landerId',
        ]);
    }

    public static function getNameColumns(): array
    {
        return array_merge(parent::getNameColumns(), [
            'landerName' => self::getSchemeName(),
        ]);
    }

    public static function getIdentifier(): ?string
    {
        return 'landerId';
    }

    public static function getVoluumName(): string
    {
        return 'lander';
    }

    public static function getSchemeName(): string
    {
        return 'lander';
    }

    public function getDisabledGroups(): array
    {
        return [
            self::$modelManager->getModel(3)::getSchemeName(),
        ];
    }
}
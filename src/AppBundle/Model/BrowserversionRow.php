<?php

namespace AppBundle\Model;

class BrowserversionRow extends ReportRow
{
    public $browserVersion;

    public static function getExcludeColumns(): array
    {
        return array_merge(parent::getExcludeColumns(), []);
    }

    public static function getNameColumns(): array
    {
        return array_merge(parent::getNameColumns(), [
            'browserVersion' => self::getSchemeName(),
        ]);
    }

    public static function getIdentifier(): ?string
    {
        return 'browserVersion';
    }

    public static function getVoluumName(): string
    {
        return 'browser-version';
    }

    public static function getSchemeName(): string
    {
        return 'browser version';
    }

    public function getDisabledGroups(): array
    {
        return [
            self::$modelManager->getModel(7)::getSchemeName(),
        ];
    }
}
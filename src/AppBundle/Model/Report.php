<?php

namespace AppBundle\Model;

use AppBundle\Scheme\Mapping\MappedScheme;
use AppBundle\Scheme\Nesting\LayerNestingScheme;
use AppBundle\Scheme\Types\CollectionType;
use AppBundle\Scheme\Types\FieldBasedCollection;
use AppBundle\Scheme\Types\SchemeType;

class Report extends LayerNestingScheme
{
    public $rows;
    public $offset;
    public $limit;
    public $totalRows;
    public $totals;
    public $messages;

    public static function getMappedFields(): array
    {
        return [
            'rows' => FieldBasedCollection::class,
            'messages' => CollectionType::class,
            'totals' => SchemeType::class,
        ];
    }

    public static function initRows()
    {
        return new FieldBasedCollection(ReportRow::class);
    }

    public static function initMessages()
    {
        return new CollectionType(ReportMessage::class);
    }

    public static function initTotals()
    {
        return new SchemeType(ReportTotals::class);
    }

    public static function getLayer(): int
    {
        return 101;
    }

    protected function __construct($rows, $offset, $limit, $totalRows, $totals, $messages)
    {
        $this->rows = $rows;
        $this->offset = $offset;
        $this->limit = $limit;
        $this->totalRows = $totalRows;
        $this->totals = $totals;
        $this->messages = $messages;
    }

    public static function fromState(array $state): MappedScheme
    {
        return new self(
            self::restore('rows', $state),
            self::restore('offset', $state),
            self::restore('limit', $state),
            self::restore('totalRows', $state),
            self::restore('totals', $state),
            self::restore('messages', $state)
        );
    }
}
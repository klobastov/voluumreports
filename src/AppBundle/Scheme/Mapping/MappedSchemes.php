<?php
declare(strict_types=1);

namespace AppBundle\Scheme\Mapping;

use AppBundle\Scheme\MappedSchemesProd;
use AppBundle\Scheme\MappedSchemesTest;

class MappedSchemes
{
    private static $import = [];

    public function __construct()
    {
        self::$import = self::get();
    }

    /**
     * @return array
     */
    public static function get(): array
    {
        if (!empty(self::$import)) {
            return self::$import;
        }

        $list = MappedSchemesProd::get();
        $sub = [];
        if (!isset($_SERVER['SYMFONY_ENV'])
            || (isset($_SERVER['SYMFONY_ENV']) && $_SERVER['SYMFONY_ENV'] !== 'prod')
        ) {
            $sub = MappedSchemesTest::get();
        }

//        @todo Добавить проверку на перезапись по ключу=id
        $list += $sub;
        ksort($list);

        return $list;
    }

    public static function getOne(int $id): ?string
    {
        if (!isset(self::$import[$id])) {
            return null;
        }

        return self::$import[$id];
    }

    public static function find($className): ?int
    {
        if (false !== $id = array_search($className, self::$import, true)) {
            return $id;
        }

        return null;
    }

    public static function load(\Closure $closure): self
    {
        $object = new MappedSchemes();

        $sub = $closure();
        if (!\is_array($sub)) {
            throw new \InvalidArgumentException('Closure should return array');
        }

        $object::$import = $sub;
        ksort($object::$import);

        return $object;
    }
}
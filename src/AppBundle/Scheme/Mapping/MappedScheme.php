<?php
declare(strict_types=1);

namespace AppBundle\Scheme\Mapping;

use AppBundle\Scheme\Base\Scheme;

abstract class MappedScheme extends Scheme
{
    abstract public static function fromState(array $state): MappedScheme;

    abstract public static function getMappedFields(): array;

    public static function restore(string $key, array $state)
    {
        if (isset($state[$key])) {
            return $state[$key];
        }

        return null;
    }
}
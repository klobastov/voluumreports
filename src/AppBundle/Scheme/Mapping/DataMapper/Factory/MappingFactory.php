<?php

namespace AppBundle\Scheme\Mapping\DataMapper\Factory;

use AppBundle\Model\DynamicRow;
use AppBundle\Scheme\Mapping\MappedSchemes;
use AppBundle\Scheme\Types\Type;
use AppBundle\Service\Layers\LayerManager;
use AppBundle\Service\Model\MappedFieldsManager;

abstract class MappingFactory
{
    protected $mappedFieldsManager;
    protected $layerManager;
    protected $schemes;

    public function __construct(
        MappedFieldsManager $mappedFieldsManager,
        LayerManager $layerManager,
        MappedSchemes $schemes
    ) {
        $this->mappedFieldsManager = $mappedFieldsManager;
        $this->layerManager = $layerManager;
        $this->schemes = $schemes;
    }

    protected function map(string $mappingScheme, array $state)
    {
        $mappingFields = $mappingScheme::getMappedFields();
        $this->mappedFieldsManager->setModel($mappingScheme);

        foreach ((array)$mappingFields as $field => $type) {
            switch ($type::getType()) {
                case Type::FIELD_BASES_COLLECTION:
                    $fields = $this->mappedFieldsManager->getCollection($field)->resolve();
                    $groups = $this->mappedFieldsManager->getCollection($field)->getRequestGroups();
                    foreach ((array)$state[$field] as $key => $element) {
                        $state[$field][$key] = new DynamicRow($element, $fields, $groups);

//                        $children = $this->layerManager->getChildren($scheme);
//                        if (!empty($children) && null !== $id = $this->schemes::find(\get_class($state[$field][$key]))) {
//                            $this->map($this->schemes::getOne($id), $state[$field][$key]);
//                        }
                    }

                    break;
                case Type::EXTENDED_COLLECTION:
                case Type::COLLECTION:
                    $scheme = $this->mappedFieldsManager->getCollection($field)->resolve();

                    foreach ((array)$state[$field] as $key => $element) {
                        $state[$field][$key] = $scheme::fromState($element);

                        $children = $this->layerManager->getChildren($scheme);
                        if (!empty($children) && null !== $id = $this->schemes::find(\get_class($state[$field][$key]))) {
                            $this->map($this->schemes::getOne($id), $state[$field][$key]);
                        }
                    }

                    break;
                case Type::SCHEME:

                    $scheme = $this->mappedFieldsManager->getScheme($field)->resolve();
                    $state[$field] = $scheme::fromState($state[$field]);

                    $children = $this->layerManager->getChildren($scheme);
                    if (!empty($children) && null !== $id = $this->schemes::find(\get_class($state[$field]))) {
                        $this->map($this->schemes::getOne($id), $state - [$field]);
                    }

                    break;
                default:
                    throw new \RuntimeException('Undefined type');
            }
        }

        return $mappingScheme::fromState($state);
    }
}
<?php

namespace AppBundle\Scheme\Mapping\DataMapper\Factory;

use AppBundle\Client\Request\VoluumRequest;
use AppBundle\Model\Error;
use AppBundle\Model\Report;
use AppBundle\Scheme\Mapping\Bridge\ArrayFromResponseFormatter;
use AppBundle\Scheme\Mapping\Bridge\ClosureFormatter;
use AppBundle\Scheme\Mapping\Bridge\Exception\UnsupportedState;
use AppBundle\Scheme\Mapping\Bridge\FormatterInterface;
use AppBundle\Scheme\Mapping\Bridge\StateParamsManager;
use AppBundle\Scheme\Mapping\Bridge\StateService;
use AppBundle\Scheme\Mapping\MappedSchemes;
use AppBundle\Service\Layers\LayerManager;
use AppBundle\Service\Model\MappedFieldsManager;
use AppBundle\Service\Model\ModelManager;

class ReportFactory extends MappingFactory
{
    private $modelManager;
    private $stateParamsManager;

    public function __construct(
        MappedFieldsManager $mappedFieldsManager,
        LayerManager $layerManager,
        MappedSchemes $schemes,
        ModelManager $modelManager,
        StateParamsManager $stateParamsManager
    ) {
        parent::__construct($mappedFieldsManager, $layerManager, $schemes);

        $this->modelManager = $modelManager;
        $this->stateParamsManager = $stateParamsManager;
    }

    public function createFromRequest(VoluumRequest $voluumRequest)
    {
        $voluumRequest->send();

        $requestUrl = $voluumRequest->getUrl();
        $requestToken = $voluumRequest->getToken();
        $requestResponse = $voluumRequest->getResponse();
        $requestContent = (new ArrayFromResponseFormatter($requestResponse))->format();

        if (empty($requestContent)) {
            return $this->error([
                'error' => [
                    'code' => $requestResponse->status_code,
                    'description' => $requestResponse->body,
                ],
                'reportRequest' => $requestUrl,
                'voluumToken' => $requestToken,
            ]);
        }


        $this->stateParamsManager->setParam('reportRequest', $requestUrl);

        return $this->createReport(new ClosureFormatter(function () use ($requestUrl, $requestToken, $requestContent) {
            $additionsReport = [];
            $additionsError = [
                'reportRequest' => $requestUrl,
                'voluumToken' => $requestToken,
            ];

            $state = array_merge(
                $additionsReport,
                $additionsError,
                $requestContent
            );

            if (!isset($state['rows']) || !\is_array($state['rows'])) {
                return $state;
            }

            //Добавляем менеджер в модели
            $state['rows'] = \array_map(function (&$row) {
                $row['modelManager'] = $this->modelManager;

                return $row;
            }, $state['rows']);


            return $state;
        }));
    }

    /**
     * @param FormatterInterface $formatter
     * @return Report|Error
     */
    private function createReport(FormatterInterface $formatter)
    {
        $stateService = new StateService(Report::class, $formatter);
        $stateService->setLayerManager($this->layerManager);
        $stateService->setMappedFieldsManager($this->mappedFieldsManager);
        $stateService->setParamsManager($this->stateParamsManager);
        $stateService->setModelManager($this->modelManager);

        try {
            $state = $stateService->get();

            $report = $this->map(Report::class, $state);
        } catch (UnsupportedState $exception) {
            $stateService->setScheme(Error::class);
            $state = $stateService->get();

            $report = $this->map(Error::class, $state);
        }

        return $report;
    }

    private function error(array $state)
    {
        return $this->map(Error::class, $state);
    }
}
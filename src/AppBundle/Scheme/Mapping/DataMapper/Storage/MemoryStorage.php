<?php
declare(strict_types=1);

namespace AppBundle\Scheme\Mapping\DataMapper\Storage;

class MemoryStorage implements StorageInterface
{
    private $data;

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function find(int $id)
    {
        if (!isset($this->data[$id])) {
            return null;
        }

        return $this->data[$id];
    }
}
<?php
declare(strict_types=1);

namespace AppBundle\Scheme\Mapping\DataMapper\Storage;

interface StorageInterface
{
    public function __construct(array $data);
    
    public function find(int $id);
}
<?php
declare(strict_types=1);

namespace AppBundle\Scheme\Mapping\Bridge;

class ClosureFormatter implements FormatterInterface
{
    private $result;

    public function __construct(\Closure $closure)
    {
        if (false === \is_array($closureResult = $closure())) {
            throw new \InvalidArgumentException('Closure should return array');
        }

        $this->result = $closureResult;
    }

    public function format(): array
    {
        return $this->result;
    }
}
<?php

namespace AppBundle\Scheme\Mapping\Bridge\Exception;

use Exception;
use Throwable;

class UnsupportedState extends Exception
{
    public function __construct($field, $code = 0, Throwable $previous = null)
    {
        $message = "Scheme wait in '{$field}' field another state for trim";
        parent::__construct($message, $code, $previous);
    }
}
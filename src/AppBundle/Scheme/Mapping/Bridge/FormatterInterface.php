<?php
declare(strict_types=1);

namespace AppBundle\Scheme\Mapping\Bridge;

interface FormatterInterface
{
    public function format(): array;
}
<?php
declare(strict_types=1);

namespace AppBundle\Scheme\Mapping\Bridge;

class ArrayFromJsonFormatter implements FormatterInterface
{
    protected $json;

    public function __construct(string $json)
    {
        $this->json = $json;
    }

    public function format(): array
    {
        $decoded = json_decode($this->json, true);
        if(\is_array($decoded)){
            return $decoded;
        }

        return [];
    }
}
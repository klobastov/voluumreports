<?php
declare(strict_types=1);

namespace AppBundle\Scheme\Mapping\Bridge;

use Requests_Response;

class ArrayFromResponseFormatter implements FormatterInterface
{
    protected $response;

    public function __construct(Requests_Response $response)
    {
        $this->response = $response;
    }

    public function format(): array
    {
        $decoded = json_decode($this->response->body, true);
        if(\is_array($decoded)){
            return $decoded;
        }

        return [];
    }
}
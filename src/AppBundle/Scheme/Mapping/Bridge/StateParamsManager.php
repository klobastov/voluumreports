<?php

namespace AppBundle\Scheme\Mapping\Bridge;

class StateParamsManager
{
    /*
     * Функция для временного хранения каких-то значений, которые следует передать
     * Например в
     * @see StateService
     * Чтобы ращрешить какую схему использовать в ExtendedCollection
     */
    private $params;

    public function setParam($name, $value): void
    {
        $this->params[$name] = $value;
    }

    public function getParam($name)
    {
        if (isset($this->params[$name])) {
            return $this->params[$name];
        }

        throw new \RuntimeException('Param not exist');
    }
}
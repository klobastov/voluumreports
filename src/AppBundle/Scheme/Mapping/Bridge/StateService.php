<?php
declare(strict_types=1);

namespace AppBundle\Scheme\Mapping\Bridge;

use AppBundle\Scheme\Mapping\Bridge\Exception\UnsupportedState;
use AppBundle\Scheme\Service\Service;
use AppBundle\Scheme\Types\CollectionType;
use AppBundle\Scheme\Types\ExtendedCollection;
use AppBundle\Scheme\Types\SchemeType;
use AppBundle\Scheme\Types\Type;
use AppBundle\Service\Layers\LayerManager;
use AppBundle\Service\Model\MappedFieldsManager;
use AppBundle\Service\Model\ModelManager;

class StateService extends Service
{
    /**
     * @var array
     */
    protected $state;
    /**
     * @var string
     */
    protected $scheme;
    /**
     * @var \Closure|null
     */
    private $beforeTrimCollection;
    /**
     * @var \Closure|null
     */
    private $afterTrimCollection;
    /**
     * @var \Closure|null
     */
    private $whenTrimComplete;
    /**
     * @var LayerManager
     */
    private $layerManager;
    /**
     * @var MappedFieldsManager
     */
    private $mappedFieldsManager;
    /**
     * @var StateParamsManager
     */
    private $paramsManager;
    private $modelManager;


    public function __construct(string $scheme, FormatterInterface $formatter)
    {
        $this->scheme = $scheme;

        parent::__construct($formatter);
        $this->state = $this->implementation->format();
    }

    public function setScheme(string $scheme): void
    {
        $this->scheme = $scheme;
        $this->mappedFieldsManager->setModel($scheme);
    }

    public function setLayerManager(LayerManager $layerManager)
    {
        $this->layerManager = $layerManager;
    }

    public function setMappedFieldsManager(MappedFieldsManager $mappedFieldsManager)
    {
        $this->mappedFieldsManager = $mappedFieldsManager;
    }

    public function setModelManager(ModelManager $modelManager)
    {
        $this->modelManager = $modelManager;
    }

    public function setParamsManager(StateParamsManager $paramsManager)
    {
        $this->paramsManager = $paramsManager;
    }

    public function get(): array
    {
        if (null === $this->layerManager) {
            throw new \RuntimeException('Add layer manager');
        }
        if (null === $this->mappedFieldsManager) {
            throw new \RuntimeException('Add mapped fields manager');
        }
        if (null === $this->paramsManager) {
            throw new \RuntimeException('Add state params manager');
        }

        //@todo устанавливать модель каждый раз когда меняется
        $this->mappedFieldsManager->setModel($this->scheme);

        $this->trim($this->state);
        $this->prepareStateOnComplete($this->state);

        return $this->state;
    }

    /**
     * Обработка, когда полностью закончится обрезка.
     * Параметры кложура (array &$state)
     * <pre>
     * $stateService->doWhenTrimComplete(function (array &$state) {
     *     $state = array_merge($state, [
     *         'reportRequest' => $this->reportRequest,
     *         'voluumToken' => $this->voluumToken,
     *     ]);
     * });
     * </pre>
     * @param \Closure|null $closure
     */
    public function doWhenTrimComplete(\Closure $closure = null)
    {
        $this->whenTrimComplete = $closure;
    }

    /**
     * Обработка перед обрезкой коллекци.
     * Параметры кложура (CollectionType $collection, array &$state)
     * <pre>
     * $stateService->doBeforeTrimCollection(function (CollectionType $collection, array &$state) {
     *     foreach ($state as &$row) {
     *         $row['reportRequest'] = $this->reportRequest;
     *     }
     * });
     * </pre>
     * @param \Closure|null $closure
     */
    public function doBeforeTrimCollection(\Closure $closure = null)
    {
        $this->beforeTrimCollection = $closure;
    }

    /**
     * Обработка после обрезки коллекции.
     * Параметры кложура (CollectionType $collection, array &$state)
     * @param \Closure|null $closure
     * @see doBeforeTrimCollection
     */
    public function doAfterTrimCollection(\Closure $closure = null)
    {
        $this->afterTrimCollection = $closure;
    }

    public function prepareStateOnComplete(array &$state)
    {
        if (\is_callable($this->whenTrimComplete)) {
            \call_user_func_array($this->whenTrimComplete, [&$state]);
        }
    }

    private function prepareCollectionBeforeTrim(CollectionType $collection, array &$state)
    {
        if (\is_callable($this->beforeTrimCollection)) {
            \call_user_func_array($this->beforeTrimCollection, [&$collection, &$state]);
        }
    }

    private function prepareCollectionAfterTrim(CollectionType $collection, array &$state)
    {
        if (\is_callable($this->afterTrimCollection)) {
            \call_user_func_array($this->afterTrimCollection, [&$collection, &$state]);
        }
    }


    private function trim(array &$state)
    {
        $fields = $this->scheme::getMappedFields();
        foreach ((array)$fields as $field => $type) {
            if (!isset($state[$field])) {
                throw new UnsupportedState($field);
            }

            switch ($type::getType()) {
                case Type::FIELD_BASES_COLLECTION:
                    $collection = $this->mappedFieldsManager->getCollection($field);
                    $collection->setStateParamsManager($this->paramsManager);
                    $collection->setModelManager($this->modelManager);
                    $this->mappedFieldsManager->addState($field, $state[$field]);

                    break;
                case Type::EXTENDED_COLLECTION:
                    $collection = $this->mappedFieldsManager->getCollection($field);
                    $collection->setStateParamsManager($this->paramsManager);
                    $collection->setModelManager($this->modelManager);
                    $this->trimCollection($collection, $state[$field]);
                    $this->mappedFieldsManager->addState($field, $state[$field]);

                    break;
                case Type::COLLECTION:
                    $collection = $this->mappedFieldsManager->getCollection($field);
                    $this->trimCollection($collection, $state[$field]);
                    $this->mappedFieldsManager->addState($field, $state[$field]);

                    break;
                case Type::SCHEME:
                    $scheme = $this->mappedFieldsManager->getScheme($field);
                    $this->trimScheme($scheme, $state[$field]);
                    $this->mappedFieldsManager->addState($field, $state[$field]);

                    break;
                default:
                    throw new \RuntimeException('Undefined type');
            }
        }
        unset($fields);

        $fields = $this->scheme::getFieldsName();
        $this->trimLees(array_diff_key($state, $fields), $fields, $state);
    }

    /**
     * @param CollectionType|ExtendedCollection $collection
     * @param array $state
     * @throws \RuntimeException
     */
    private function trimCollection(CollectionType $collection, array &$state)
    {
        $this->prepareCollectionBeforeTrim($collection, $state);

        $scheme = $collection->resolve();
        if (null === $scheme) {
            throw new \RuntimeException('Invalid collection type');
        }

        $fields = $scheme::getFieldsName();
        foreach ($state as $key => &$element) {
            foreach ((array)$element as $field => $value) {
                if (!\in_array($field, $fields, true)) {
                    unset($state[$key][$field]);
                    continue;
                }

                //если внутри колллекции схема, чистим её
                //@todo додедать до mappedFieldsManager
                if ($this->validateFieldOnScheme($fields, $state[$key][$field])) {
                    throw new \RuntimeException('недоделано');
                    $this->trimScheme($fields, $state[$key][$field]);
                }

                //если внутри коллекци ещё коллеция
                //@todo додедать до mappedFieldsManager
                if ($this->validateFieldOnCollection($state[$key][$field])) {
                    throw new \RuntimeException('недоделано');
                    $this->trimCollection($fields[$field], $state[$key][$field]);
                }
            }
            //удаляем коллецию, если в ней нет ничего валидного
            if (empty($state[$key])) {
                unset($state[$key]);
            }
        }
        unset($element);

        $this->prepareCollectionAfterTrim($collection, $state);
    }

    private function trimScheme(SchemeType $type, array &$state)
    {
        $scheme = $type->resolve();

        $fields = $scheme::getFieldsName();

        foreach ($state as $field => $value) {
            if (!\in_array($field, $fields, true)) {
                unset($state[$field]);
                continue;
            }

            $children = $this->layerManager->getChildren($scheme);
            foreach ((array)$children as $child) {
                //если есть внутри схемы ещё схемы, чистим и их
                //@todo додедать до mappedFieldsManager
                if ($this->validateFieldOnScheme($child::getFieldsName(), $state[$field])) {
                    throw new \RuntimeException('недоделано');
                    $this->trimScheme($child, $state[$field]);
                }

                //если внутри схемы есть коллекции, чистим каждую
                //@todo додедать до mappedFieldsManager
                if ($this->validateFieldOnCollection($state[$field])) {
                    throw new \RuntimeException('недоделано');
                    $this->trimCollection($fields[$field], $state[$field]);
                }
            }
        }
    }

    private function trimLees(array $lees, $allowed, array &$state)
    {
        foreach ($lees as $field => $item) {
            if (!\in_array($field, $allowed, true)) {
                unset($state[$field]);
            }
        }
    }

    private function validateFieldOnScheme(array $fields, $state)
    {
        if (!\is_array($state)) {
            return false;
        }

        $arrays = 0;
        $rows = 0;
        foreach ($state as $item) {
            if (\is_array($item)) {
                $arrays++;
                continue;
            }
            $rows++;
        }
        if (!$rows && $arrays) {
            return false;
        }

        foreach ($fields as $field => $value) {
            if (!isset($state[$field])) {
                return false;
            }
        }

        return true;
    }

    private function validateFieldOnCollection($state)
    {
        if (!\is_array($state)) {
            return false;
        }

        $arrays = 0;
        $rows = 0;
        foreach ($state as $item) {
            if (\is_array($item)) {
                $arrays++;
                continue;
            }
            $rows++;
        }

        return !$rows && $arrays;
    }
}
<?php
declare(strict_types=1);

namespace AppBundle\Scheme;

use AppBundle\Model\Error;
use AppBundle\Model\ErrorMessage;
use AppBundle\Model\Report;
use AppBundle\Model\ReportMessage;
use AppBundle\Model\ReportRow;
use AppBundle\Model\ReportTotals;

class MappedSchemesProd
{
    //MODELS
    protected const VOLUUM_REPORT = 1;
    protected const VOLUUM_REPORT_TOTAL = 2;
    protected const VOLUUM_REPORT_ROW = 6;
    protected const VOLUUM_REPORT_MESSAGE = 3;
    protected const VOLUUM_ERROR = 4;
    protected const VOLUUM_ERROR_MESSAGE = 5;

    private static $import = [
        //MODELS
        self::VOLUUM_REPORT => Report::class,
        self::VOLUUM_REPORT_TOTAL => ReportTotals::class,
        self::VOLUUM_REPORT_ROW => ReportRow::class,
        self::VOLUUM_REPORT_MESSAGE => ReportMessage::class,
        self::VOLUUM_ERROR => Error::class,
        self::VOLUUM_ERROR_MESSAGE => ErrorMessage::class,
    ];

    /**
     * @return array
     */
    public static function get(): array
    {
        $list = self::$import;
        ksort($list);

        return $list;
    }
}
<?php
declare(strict_types=1);

namespace AppBundle\Scheme\Nesting;

use AppBundle\Scheme\Mapping\MappedScheme;

abstract class LayerNestingScheme extends MappedScheme
{
    abstract public static function getLayer(): int;
}
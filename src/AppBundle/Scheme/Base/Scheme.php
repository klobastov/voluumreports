<?php
declare(strict_types=1);

namespace AppBundle\Scheme\Base;

abstract class Scheme
{
    public static function getFieldsName()
    {
        return array_map(function (\ReflectionProperty $property) {
            return $property->getName();
        }, (new \ReflectionClass(static::class))->getProperties(\ReflectionProperty::IS_PUBLIC));
    }
}
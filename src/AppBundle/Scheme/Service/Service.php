<?php
declare(strict_types=1);

namespace AppBundle\Scheme\Service;

use AppBundle\Scheme\Mapping\Bridge\FormatterInterface;

abstract class Service
{
    protected $implementation;

    abstract public function get(): array;

    public function __construct(FormatterInterface $formatter)
    {
        $this->implementation = $formatter;
    }

    public function setImplementation(FormatterInterface $formatter): void
    {
        $this->implementation = $formatter;
    }
}
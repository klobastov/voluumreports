<?php
declare(strict_types=1);

namespace AppBundle\Scheme;

use AppBundle\Model\BrandRow;
use AppBundle\Model\BrowserRow;
use AppBundle\Model\BrowserversionRow;
use AppBundle\Model\CampaignRow;
use AppBundle\Model\CountrycodeRow;
use AppBundle\Model\DeviceRow;
use AppBundle\Model\LanderRow;
use AppBundle\Model\MobilecarrierRow;
use AppBundle\Model\ModelRow;
use AppBundle\Model\OfferRow;
use AppBundle\Model\OsRow;
use AppBundle\Model\OsversionRow;
use AppBundle\Model\ReferrerdomainRow;
use AppBundle\Model\TrafficsourceRow;

class MappedModels
{
    //MODELS
    protected const MODEL_CAMPAIGN = 1;
    protected const MODEL_OFFER = 2;
    protected const MODEL_LANDER = 3;
    protected const MODEL_COUNTRY = 4;
    protected const MODEL_MOBILECARRIER = 5;
    protected const MODEL_BROWSER = 6;
    protected const MODEL_BROWSER_VERSION = 7;
    protected const MODEL_OS = 8;
    protected const MODEL_OSVERSION = 9;
    protected const MODEL_DEVICE = 10;
    protected const MODEL_BRAND = 11;
    protected const MODEL_MODEL = 12;
    protected const MODEL_REFERRERDOMAIN = 13;
    protected const MODEL_TRAFFICSOURCE = 14;

    private static $import = [
        //MODELS
        self::MODEL_CAMPAIGN => CampaignRow::class,

        self::MODEL_OFFER => OfferRow::class,

        self::MODEL_LANDER => LanderRow::class,

        self::MODEL_COUNTRY => CountrycodeRow::class,

        self::MODEL_MOBILECARRIER => MobilecarrierRow::class,

        self::MODEL_BROWSER => BrowserRow::class,

        self::MODEL_BROWSER_VERSION => BrowserversionRow::class,

        self::MODEL_OS => OsRow::class,

        self::MODEL_OSVERSION => OsversionRow::class,

        self::MODEL_DEVICE => DeviceRow::class,

        self::MODEL_BRAND => BrandRow::class,

        self::MODEL_MODEL => ModelRow::class,

        self::MODEL_REFERRERDOMAIN => ReferrerdomainRow::class,

        self::MODEL_TRAFFICSOURCE => TrafficsourceRow::class,
    ];

    /**
     * @return array
     */
    public static function get(): array
    {
        $list = self::$import;
        ksort($list);

        return $list;
    }
}
<?php
declare(strict_types=1);

namespace AppBundle\Scheme;

class MappedSchemesTest
{
//    //FIXTURES FOR TEST
//    protected const CAMPAIGN = 4001;
//    protected const FLOW = 4002;
//    protected const STREAM = 4003;
//    protected const ACTION = 4004;
//    protected const CONDITION = 4005;
//    protected const CAMPAIGN_LINEAR = 1001;
//    protected const FLOW_LINEAR = 1002;
//    protected const STREAM_LINEAR = 1003;
//    protected const ACTION_LINEAR = 1004;
//    protected const CAMPAIGN_ONE_COLLECTION = 2001;
//    protected const FLOW_ONE_COLLECTION = 2002;
//    protected const STREAM_ONE_COLLECTION = 2003;
//    protected const ACTION_ONE_COLLECTION = 2004;
//    protected const CAMPAIGN_NESTED_COLLECTION = 3001;
//    protected const FLOW_NESTED_COLLECTION = 3002;
//    protected const STREAM_NESTED_COLLECTION = 3003;
//    protected const ACTION_NESTED_COLLECTION = 3004;
//    protected const CONDITION_NESTED_COLLECTION = 3005;
//
//    private static $import = [
//        //FIXTURES FOR TESTS
//        //Base
//        self::CAMPAIGN => Campaign::class,
//        self::FLOW => Flow::class,
//        self::STREAM => Stream::class,
//        self::ACTION => Action::class,
//        self::CONDITION => Condition::class,
//        //Linear
//        self::CAMPAIGN_LINEAR => CampaignLinear::class,
//        self::FLOW_LINEAR => FlowLinear::class,
//        self::STREAM_LINEAR => StreamLinear::class,
//        self::ACTION_LINEAR => ActionLinear::class,
//        //OneCollection
//        self::CAMPAIGN_ONE_COLLECTION => CampaignOneCollection::class,
//        self::FLOW_ONE_COLLECTION => FlowOneCollection::class,
//        self::STREAM_ONE_COLLECTION => StreamOneCollection::class,
//        self::ACTION_ONE_COLLECTION => ActionOneCollection::class,
//        //NestedCollection
//        self::CAMPAIGN_NESTED_COLLECTION => CampaignNestedCollection::class,
//        self::FLOW_NESTED_COLLECTION => FlowNestedCollection::class,
//        self::STREAM_NESTED_COLLECTION => StreamNestedCollection::class,
//        self::ACTION_NESTED_COLLECTION => ActionNestedCollection::class,
//        self::CONDITION_NESTED_COLLECTION => ConditionNestedCollection::class,
//    ];
//
//    /**
//     * @return array
//     */
    public static function get(): array
    {
//        $list = self::$import;
//        ksort($list);
//
//        return $list;
        return [];
    }
}
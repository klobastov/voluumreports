<?php

namespace AppBundle\Scheme\Types;

class SchemeType extends Type
{
    private $scheme;

    public function __construct(string $scheme)
    {
        $this->scheme = $scheme;
    }

    public function getScheme(): string
    {
        return $this->scheme;
    }

    public static function getType(): string
    {
        return self::SCHEME;
    }

    public function resolve(): string
    {
        return $this->getScheme();
    }
}
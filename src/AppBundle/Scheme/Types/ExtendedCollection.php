<?php

namespace AppBundle\Scheme\Types;

class ExtendedCollection extends CollectionType
{
    public static function getType(): string
    {
        return self::EXTENDED_COLLECTION;
    }

    public function resolve(): string
    {
        if (null === $this->stateParamsManager) {
            throw new \RuntimeException('Set state params manager');
        }

        if (null === $this->modelManager) {
            throw new \RuntimeException('Set model manager');
        }

        $classNames = $this->prepareClassNames();
        if (!empty($classNames)) {
            return $classNames[0];
        }

        return parent::resolve();
    }

    public function getRequestGroups()
    {
        $reportRequest = $this->stateParamsManager->getParam('reportRequest');

        $query = \parse_url($reportRequest, \PHP_URL_QUERY);
        $paramsRaw = \explode('&', $query);

        $params = \array_filter($paramsRaw, function ($paramRaw, $key) {
            return false !== \strpos($paramRaw, 'groupBy');
        }, ARRAY_FILTER_USE_BOTH);

        return array_values(\array_map(function ($param) {
            return \explode('=', $param)[1];
        }, $params));
    }

    private function prepareClassNames()
    {
        $classNames = [];

        $groups = $this->getRequestGroups();

        $limitGroups = \count($groups);
        if ($limitGroups === 1) {
            $name = \ucfirst($groups[0]).'Row';
            $name = \str_replace('-', '', $name);
            $name = 'AppBundle\\Model\\'.$name;
            if ($this->modelManager->classExist($name)) {
                $classNames[] = $name;
            }
        } elseif ($limitGroups === 2) {
            for ($i = 0; $i < $limitGroups; $i++) {
                for ($j = 0; $j < $limitGroups; $j++) {
                    if ($groups[$i] !== $groups[$j]) {
                        $name = \ucfirst($groups[$i]).'GroupBy'.\ucfirst($groups[$j]).'Row';
                        $name = \str_replace('-', '', $name);
                        $name = 'AppBundle\\Model\\'.\str_replace('-', '', \ucfirst($groups[$i])).'\\'.$name;
                        if ($this->modelManager->classExist($name)) {
                            $classNames[] = $name;
                            break;
                        }
                    }
                }
            }
        } elseif ($limitGroups === 3) {
            for ($i = 0; $i < $limitGroups; $i++) {
                for ($j = 0; $j < $limitGroups; $j++) {
                    for ($k = 0; $k < $limitGroups; $k++) {
                        if ($groups[$i] !== $groups[$j] && $groups[$j] !== $groups[$k] && $groups[$i] !== $groups[$k]) {
                            $name = \ucfirst($groups[$i]).'GroupBy'.\ucfirst($groups[$j]).\ucfirst($groups[$k]).'Row';
                            $name = \str_replace('-', '', $name);
                            $name = 'AppBundle\\Model\\'.\ucfirst($groups[$i]).'\\'.\ucfirst($groups[$j]).'\\'.$name;
                            if ($this->modelManager->classExist($name)) {
                                $classNames[] = $name;
                                break;
                            }
                        }
                    }
                }
            }
        }

        return $classNames;
    }
}
<?php

namespace AppBundle\Scheme\Types;

abstract class Type
{
    /**
     * @var array
     */
    protected $state;
    /**
     * Доступные типы
     */
    public const FIELD_BASES_COLLECTION = 'field_based_collection';
    public const EXTENDED_COLLECTION = 'extended_collection';
    public const COLLECTION = 'collection';
    public const SCHEME = 'scheme';

    abstract public static function getType(): string;

    abstract public function resolve();

    public function setState(array $state)
    {
        $this->state = $state;

        return $this;
    }

    public function getState(): array
    {
        return $this->state;
    }
}
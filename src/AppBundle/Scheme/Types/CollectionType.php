<?php

namespace AppBundle\Scheme\Types;

use AppBundle\Scheme\Mapping\Bridge\StateParamsManager;
use AppBundle\Service\Model\ModelManager;

class CollectionType extends Type
{
    private $scheme;
    /**
     * @var StateParamsManager
     */
    protected $stateParamsManager;
    /**
     * @var ModelManager
     */
    protected $modelManager;

    public function __construct(string $scheme)
    {
        $this->scheme = $scheme;
    }

    public function getScheme(): string
    {
        return $this->scheme;
    }

    public function resolve(): string
    {
        return $this->getScheme();
    }

    public static function getType(): string
    {
        return self::COLLECTION;
    }

    public function setStateParamsManager(StateParamsManager $stateParamsManager)
    {
        $this->stateParamsManager = $stateParamsManager;
    }

    public function setModelManager(ModelManager $modelManager)
    {
        $this->modelManager = $modelManager;
    }
}
<?php

namespace AppBundle\Scheme\Types;

class FieldBasedCollection extends ExtendedCollection
{
    public static function getType(): string
    {
        return self::FIELD_BASES_COLLECTION;
    }

    /**
     * @return array|string
     * @throws \RuntimeException
     */
    public function resolve()
    {
        if (null === $this->stateParamsManager) {
            throw new \RuntimeException('Set state params manager');
        }

        if (null === $this->modelManager) {
            throw new \RuntimeException('Set model manager');
        }

        $groups = $this->getRequestGroups();
        $allGroupFields = $this->getFieldsPerGroup($groups);

        $fields = [];
        foreach ($allGroupFields as $groupFields) {
            $fields += \array_flip($groupFields);
        }

        return \array_keys($fields);
    }

    private function getModelClassByVoluumName($voluumName)
    {
        //Подготовки к внутренниму виду назвнаий моделей в проекте
        $name = \ucfirst($voluumName).'Row';
        $name = \str_replace('-', '', $name);

        $fileName = 'AppBundle\\Model\\'.$name;
        if ($this->modelManager->classExist($fileName)) {
            return $fileName;
        }
    }

    private function getFieldsPerGroup(array $groups)
    {
        $fields = [];
        foreach ($groups as $group) {
            $groupFields = $this->modelManager->getFromAll(function (string $model) use ($group) {
                if ($model === $this->getModelClassByVoluumName($group)) {
                    return $model::getFieldsName();
                }
            });

            $groupFields = \array_filter($groupFields, function ($fields) {
                return null !== $fields;
            });

            $fields[$group] = \array_shift($groupFields);
        }

        return $fields;
    }
}
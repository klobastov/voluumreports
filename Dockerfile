FROM php:7.1-fpm

ENV LANG="en_US.UTF-8" \
    LC_ALL="en_US.UTF-8" \
    LANGUAGE="en_US.UTF-8" \
    DEBIAN_FRONTEND="noninteractive" \
    SYMFONY_ENV="prod" \
    SYMFONY_DEBUG="0" \
    COMPOSER_ALLOW_SUPERUSER=1

WORKDIR /var/www/voluumreports

RUN apt-get update -q && \
    apt-get install -qy \
        locales && \
    export LC_ALL=en_US.UTF-8 && \
    export LANG=en_US.UTF-8 && \
    cp /usr/share/zoneinfo/UTC /etc/localtime && echo "UTC" > /etc/timezone && \
    echo 'en_US.UTF-8 UTF-8' > /etc/locale.gen && \
    locale-gen en_US en_US.UTF-8 && \
    dpkg-reconfigure locales && \
    curl -sL https://deb.nodesource.com/setup_8.x | bash - && \
    curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - && \
    curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer && \
    echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list && \
    apt-get update -q && \
    apt-get install -qy --no-install-recommends \
        less \
        libzip-dev \
        tzdata \
        nodejs \
        libjpeg62-turbo \
        libpng12-0 \
        git && \
    npm install -g \
        gulp-cli && \
    pecl install \
        zip && \
    docker-php-ext-install \
        opcache && \
    docker-php-ext-enable \
        zip \
        opcache && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* && \
    mkdir -p src/AppBundle/Resources && \
    mkdir -p web/build

EXPOSE 9000

ADD /docker/php/prod/php.ini /usr/local/etc/php/
COPY /docker/php/prod/pool.conf /usr/local/etc/php-fpm.d/www.conf

COPY app /var/www/voluumreports/app
COPY bin /var/www/voluumreports/bin
COPY web /var/www/voluumreports/web

ADD /var/SymfonyRequirements.php /var/www/voluumreports/var/
ADD /var/bootstrap.php.cache /var/www/voluumreports/var/
ADD composer.json /var/www/voluumreports/
ADD composer.lock /var/www/voluumreports/
RUN php /usr/local/bin/composer install --no-interaction --no-ansi --no-dev && \
    php /usr/local/bin/composer dump-autoload --optimize

RUN chmod -R 777 ./var

ADD package-lock.json /var/www/voluumreports/
ADD package.json /var/www/voluumreports/
ADD webpack.config.js /var/www/voluumreports/
ADD gulpfile.js /var/www/voluumreports/
RUN npm install && \
    gulp build

ADD phpunit.xml.ci /var/www/voluumreports/
COPY tests /var/www/voluumreports/tests
COPY src /var/www/voluumreports/src

VOLUME /var/www/voluumreports
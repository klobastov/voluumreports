import 'daterangepicker';
import 'fix-table-header';
import moment from 'moment';

import Cookie from './helpers/cookie';
import alert from './helpers/alert';
import GETParams from './helpers/GETParams';

const getParams = new GETParams(window.location.search);

$(window).on('load', () => {
    moment().format('YYYY-MM-DD[T]H:mm:ss[Z]ZZ');
    const TIMEZONES = {
        'Asia/Tomsk': '+0700',
        'Europe/Moscow': '+0300'
    };

    const DATEFORMAT = 'YYYY-MM-DD[T]H:mm:ss[Z]ZZ';

    function getKey(obj, val) {
        for (let item in obj) {
            if (obj[item] === val) return item;
        }
    }

    function getDateURL(param) {
        if (!getParams.get(param)) {
            return undefined;
        } else {
            return getParams.get(param) + "Z" + TIMEZONES[getParams.get('tz')];
        }
    }

   

    /**
     * Инициализирует daterangepicker c нужными параметрами
     *
     * @name initDaterangepicker
     * @function
     * @param selector {jQuery DOM-element} - Элемент на котором происходит инициализация
     * @param start {moment.js object} - дата начала периода
     * @param end {moment.js object} - дата конца периода
     * @param fromInput {moment.js object} - Скрытое поле, в которое помещается дата начала периода
     * @param toInput {moment.js object} - Скрытое поле, в которое помещается дата конца периода
     * @param callback {function} - Callback
     */
    function initDaterangepicker(selector, start, end, fromInput, toInput, callback) {
        selector.daterangepicker({
            startDate: !!start ? start.utcOffset(Cookie.get('timezone')) : moment().utcOffset(Cookie.get('timezone')),
            endDate: !!start ? end.utcOffset(Cookie.get('timezone')) : moment().utcOffset(Cookie.get('timezone')),
            opens: "right",
            alwaysShowCalendars: true,
            showCustomRangeLabel: false,
            autoUpdateInput: true,
            timePicker: true,
            timePickerIncrement: 60,
            timePicker24Hour: true,
            autoApply: true,
            ranges: {
                'Today': [
                    moment().set('hour', 0).set('minute', 0).set('second', 0),
                    moment().set('hour', 0).set('minute', 0).set('second', 0).add(1, 'days')
                ],
                'Yesterday': [
                    moment().set('hour', 0).set('minute', 0).set('second', 0).subtract(1, 'days'),
                    moment().set('hour', 0).set('minute', 0).set('second', 0)
                ],
                'Before Yesterday': [
                    moment().set('hour', 0).set('minute', 0).set('second', 0).subtract(2, 'days'),
                    moment().set('hour', 0).set('minute', 0).set('second', 0).subtract(1, 'days')
                ],
                'Last 3 Days': [
                    moment().set('hour', 0).set('minute', 0).set('second', 0).subtract(3, 'days').add(1, 'days'),
                    moment().set('hour', 0).set('minute', 0).set('second', 0).add(1, 'days')
                ],
                'Last 7 Days': [
                    moment().set('hour', 0).set('minute', 0).set('second', 0).subtract(7, 'days').add(1, 'days'),
                    moment().set('hour', 0).set('minute', 0).set('second', 0).add(1, 'days')
                ],
                'Last 30 Days': [
                    moment().set('hour', 0).set('minute', 0).set('second', 0).subtract(30, 'days').add(1, 'days'),
                    moment().set('hour', 0).set('minute', 0).set('second', 0).add(1, 'days')
                ],
                'This Month': [
                    moment().set('hour', 0).set('minute', 0).set('second', 0).startOf('month'),
                    moment().endOf('month').set('hour', 0).set('minute', 0).set('second', 0).add(1, 'days')
                ],
                'Last Month': [
                    moment().set('hour', 0).set('minute', 0).set('second', 0).subtract(1, 'month').startOf('month'),
                    moment().subtract(1, 'month').endOf('month').set('hour', 0).set('minute', 0).set('second', 0).add(1, 'days')
                ],
                'This Year': [
                    moment().set('hour', 0).set('minute', 0).set('second', 0).startOf('year'),
                    moment().endOf('year').set('hour', 0).set('minute', 0).set('second', 0).add(1, 'days')
                ],
                'Last Year': [
                    moment().set('hour', 0).set('minute', 0).set('second', 0).subtract(1, 'year').startOf('year'),
                    moment().subtract(1, 'year').endOf('year').set('hour', 0).set('minute', 0).set('second', 0).add(1, 'days')
                ]
            },
            locale: {
                format: `YYYY-MM-DD[T]H:mm:ss[Z]ZZ`
            }
        });

        selector.find('span').html(`<i class="fa fa-calendar" aria-hidden="true"></i> ${start.format('YYYY-MM-DD H:00') + ' \u2014 ' + end.format('YYYY-MM-DD H:00')} <i class="fa fa-caret-down color_blue" aria-hidden="true">`);
        selector.append($(`<input type="hidden" name="${fromInput}" value="${start.format('YYYY-MM-DD[T]H:00:00' + Cookie.get('timezone'))}">`));
        selector.append($(`<input type="hidden" name="${toInput}" value="${end.format('YYYY-MM-DD[T]H:00:00' + Cookie.get('timezone'))}">`));

        selector.on('apply.daterangepicker', (event, picker) => {
            setDaterangepickerFields(selector, picker.startDate, picker.endDate, fromInput, toInput);
        });

        //Костыль для установки парвильного времени в календаль endDate
        selector.on('show.daterangepicker', (event, picker) => {
            $('.calendar.right .hourselect option[value="' + picker.endDate.format('H') + '"]').attr("selected", "selected");
        });

        if (callback) callback();

        function setDaterangepickerFields(selector, start, end, fromInput, toInput) {
            selector.find('span').html(`<i class="fa fa-calendar" aria-hidden="true"></i> ${start.format('YYYY-MM-DD H:00') + ' \u2014 ' + end.format('YYYY-MM-DD H:00')} <i class="fa fa-caret-down color_blue" aria-hidden="true">`);
            console.log(selector.find('.' + fromInput));
            selector.find(`input[name=${fromInput}]`).val(start.format('YYYY-MM-DD[T]H:00:00' + Cookie.get('timezone')));
            selector.find(`input[name=${toInput}]`).val(end.format('YYYY-MM-DD[T]H:00:00' + Cookie.get('timezone')));
        }
    }

    function checkTimezone() {
        const timezoneSelect = $('#js-timezone-select');

        if (!Cookie.get('timezone')) {
            Cookie.set('timezone', TIMEZONES['Asia/Tomsk'], {expires: 999999});
            alert('warning', `<b>Установлен часовой пояс по умолчанию: ${getKey(TIMEZONES, Cookie.get('timezone'))}</b><br>Изменить часовой пояс можно в <a>Настройках профиля</a>`);
        }

        for (let timezone in TIMEZONES) {
            let selected = Cookie.get('timezone') === TIMEZONES[timezone] ? 'selected' : '';
            timezoneSelect.append(`<option value="${TIMEZONES[timezone]}" ${selected}>${timezone}</option>`)
        }

        timezoneSelect.on('change', (event) => {
            Cookie.set('timezone', $(event.target).val(), {expires: 999999});
            alert('success', `<b>Установлен часовой пояс: ${getKey(TIMEZONES, Cookie.get('timezone'))}</b><br>Изменения вступят в силу после <a href="javascript:void(0)" onclick="window.location.reload();">перезагрузки страницы</a>`);
        });
    }

    function generateFilter(id, name, data) {
        const FIELDS = {
            filter: 'opt',
            name: 'nm',
            relation: 'rl',
            value: 'vl'
        };

        const filterMarkup = $(`
        <div class="filter__item">
            <div class="filter__title"><i class="fa fa-filter"></i> ${name}</div>
            <div>
                <input type="hidden" name="${FIELDS.filter}[${id}][${FIELDS.name}]" value="${name}">
                <select name="${FIELDS.filter}[${id}][${FIELDS.relation}]" class="filter__select">
                    <option value="m" ${!!(data && data.relation === "m") ? 'selected="selected"' : 'selected="selected"'}>more than</option>
                    <option value="e" ${!!(data && data.relation === "e") ? 'selected="selected"' : ''}>equally</option>
                    <option value="l" ${!!(data && data.relation === "l") ? 'selected="selected"' : ''}>less than</option>
                </select>
                <div class="dash"></div>
                <input class="input" type="text" name="opt[${id}][${FIELDS.value}]" value="${data ? data.value : ""}" required>
                <button type="button" class="button button_remove filter__button js-filter-remove">
                    <i class="fa fa-times" aria-hidden="true"></i>
                </button>
            </div>
        </div>
    `);

        filterMarkup.find('.js-filter-remove').on('click', (event) => {
            deleteFilter(id, $(event.target).closest('.filter__item'), name);
        });

        $('.filter__container').append(filterMarkup);

        $('.js-filter-select option').each((index, item) => {
            if ($(item).text() === name) item.remove();
        });
    }

    function deleteFilter(id, item, name) {
        const newOption = `<option value="${id}">${name}</option>`;

        item.remove();
        $('.js-filter-select').append(newOption);
    }

/**********/
/** LOAD **/
/**********/

    $('input[name="for"]').val(getParams.get('for'));

    for (let i = 0; i < 4; i++) {
        let name = getParams.get(`groupBy[${i}]`);
        if (!name) break;
        
        let value =  getParams.get(`groupBy[${i}]`);
        
        $('.filter').append(`<input type="hidden" name="groupBy[${i}]" value="${value}">`)
    }
    
    if (getParams.checkParam('identifier')) {
        $('.filter').append(`<input type="hidden" name="identifier" value="${getParams.get('identifier')}">`);
        $('.filter').append(`<input type="hidden" name="model" value="${getParams.get('model')}">`);
        
        if (getParams.checkParam('identifier2')) {
            $('.filter').append(`<input type="hidden" name="identifier2" value="${getParams.get('identifier2')}">`);
            $('.filter').append(`<input type="hidden" name="model2" value="${getParams.get('model2')}">`);
        }
    }

    checkTimezone();

// Инициализация daterangepicker'ов    
    initDaterangepicker(
        $('#compare-from'),
        !!getParams.get('from') ? moment(getDateURL('from'), DATEFORMAT) : moment().subtract(1, 'weeks'),
        !!getParams.get('to') ? moment(getDateURL('to'), DATEFORMAT) : moment(),
        'from',
        'to'
    );

    
    if(!getParams.checkParam('from2') && !getParams.checkParam('to2')) {
        $('#compare-to').on('click', function () {
            $(this).off('click');
            initDaterangepicker(
                $('#compare-to'),
                !!getParams.get('from2') ? moment(getDateURL('from2'), DATEFORMAT) : moment().subtract(14, 'days'),
                !!getParams.get('to2') ? moment(getDateURL('to2'), DATEFORMAT) : moment().subtract(7, 'days'),
                'from2',
                'to2'
            );  
        });
    } else {
        initDaterangepicker(
            $('#compare-to'),
            !!getParams.get('from2') ? moment(getDateURL('from2'), DATEFORMAT) : moment().subtract(14, 'days'),
            !!getParams.get('to2') ? moment(getDateURL('to2'), DATEFORMAT) : moment().subtract(7, 'days'),
            'from2',
            'to2'
        );
    }
    
// Генерация выбранных фильтров из GET
        const filterAddButton = $('.js-filter-add');
    
    for (let i = 0; i <= $('.js-filter-select option').length; i++) {
        let name = getParams.get(`opt[${i}][nm]`);
        if (!name) continue;

        let data = {
            relation: getParams.get(`opt[${i}][rl]`),
            value: getParams.get(`opt[${i}][vl]`)
        };
        generateFilter(i, name, data)
    }

    $('[data-voluum-name]').each((index, item) => {
        for (let param in getParams.getAll()) {
            if ($(item).data('voluum-name') == getParams.get(param)) {
                $(item).append(`<input type="hidden" name="${param}" value="${getParams.get(param)}">`);
                $(item).addClass('active');
            }
        }
    });

    $('[data-page]').on('click', function (event) {
        if ($(event.delegateTarget).hasClass('active')) {
            return false;
        }
        getParams.add({page: $(event.delegateTarget).data('page')});
        document.location.href = getParams.toString(getParams.query);
    });

    const currentPage = $(`[data-page="${getParams.get('page')}"]`);
    currentPage.addClass('active');

    $('[data-voluum-name]').on('click', () => {
        let groupByCount = 0;
        let newGroupBy = {};
        if ($(event.target).hasClass('active')) {

            getParams.remove(getKey(getParams.getAll(), $(event.target).data('voluum-name')));
            getParams.set('page', 1);
            document.location.href = getParams.toString(getParams.query);
            return;
        }

        for (let i = 0; i < 3; i++) {
            let name = getParams.get(`groupBy[${i}]`);

            if (!name) {
                if (i == 0) {
                    groupByCount = i;
                    break;
                }
                break;
            }
            groupByCount = i + 1;
        }

        newGroupBy[`groupBy[${groupByCount}]`] = $(event.target).data('voluum-name');
        getParams.add(newGroupBy);
        getParams.set('page', 1);
        document.location.href = getParams.toString(getParams.query);
    });

//Добавление фильтра

    filterAddButton.on('click', () => {
        const filterSelect = $('.js-filter-select');
        const filterName = filterSelect.find('option:selected').text(),
            value = filterSelect.find('option:selected').val();

        if (filterSelect.find('option').length) {
            generateFilter(value, filterName);
        }
    });

//Таблица 
    const table = $('#fixed-table-wrapper');

    if ($('#fixed-table-wrapper').length) {
        table.css({
            'max-height': ($(window).innerHeight()
                - table.offset().top
                - $('.pagination').outerHeight()
                - parseInt($('.lastbox').css('padding-bottom'), 10)
                - parseInt($('#content').css('padding-bottom'), 10))
            - 20 - 10 + "px"
        });
    }

    table.fixTableHeader({
        fixHeader: true,
        fixFooter: false
    });

//Переход в подробное описание
    table.find('[data-identifier] td:first-child').prepend(`<a class="fa fa-long-arrow-right preview-link" aria-hidden="true"></a>`);
    table.find('.preview-link').on('click', (event) => {
        const identifier = $(event.target).closest('tr').data('identifier'),
            model = $(event.target).closest('tr').data('model');
        const identifier2 = $(event.target).closest('tr').data('identifier2'),
            model2 = $(event.target).closest('tr').data('model2');
        if (identifier && model) {
            let data = {
                identifier: identifier,
                model: model
            };
            getParams.add(data);
        }

        if (identifier2 && model2) {
            let data2 = {
                identifier2: identifier2,
                model2: model2
            };
            getParams.add(data2);
           
        }
        getParams.set('page', 1);
        document.location.href = getParams.toString(getParams.query);
    });
});

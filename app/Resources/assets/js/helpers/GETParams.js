class GETParams {
    constructor() {
        this.query = this.getAll();
    }

    getAll() {
        return window
            .location
            .search
            .replace('?', '')
            .split('&')
            .reduce(
                function (p, e) {
                    let a = e.split('=');
                    p[decodeURIComponent(a[0])] = decodeURIComponent(a[1]);
                    return p;
                },
                {}
            );
    }

    get(name) {
        return this.query[name]
    }

    set(key, value) {
        for (let param in this.query) {
            if (param === key) {
                this.query[key] = value;
            }
        }
        return this.query;
    }

    add(newParams) {
        let currentGET = this.query;

        for (let newParam in newParams) {
            currentGET[newParam] = newParams[newParam];
        }
        this.query = currentGET;
        return this.query;
    }

    remove(params) {
        params = this._generateArrayFromString(params);
        let currentGET = this.query;

        params.map((param) => {
            delete currentGET[param];
        });
        this.query = currentGET;
        return this.query;
    }

    checkParam(name) {
        for (let param in this.query) {
            if (param == name) return true;
        }
        return false;
    }

    toString(GETObject) {
        let string = "";
        let i = 0;

        for (let param in GETObject) {
            if (i === 0) {
                string += `?${param}=${this.get(param)}`;
                i++;
                continue;
            }

            string += `&${param}=${encodeURIComponent(GETObject[param])}`;
        }
        return string;
    }

    _generateArrayFromString(param) {
        let newParam = [];

        if (!Array.isArray(param)) {
            newParam[0] = param;
            return newParam;
        } else return param;
    }
}

export default GETParams;
export default alert = (type, text) =>  {
    const markup = $(`
        <div class="alert ${type ? 'alert-' + type : ""}" style="position: relative">
            <div class="alert__close" style="position: absolute; top: 0; right: 0; padding: 3px 10px; font-size: 20px; cursor: pointer">
                <i class="fa fa-times" aria-hidden="true"></i>
            </div>
            ${text}
        </div>
    `);
    
    markup.find('.alert__close').on('click', (event) => {
       $(event.target).closest('.alert').remove();
    });

    $('.content-wrapper').find('.alert').remove();
    $('.content-wrapper').prepend($(markup));
}
const gulp = require('gulp'),
    run = require('gulp-run'),
    image = require('gulp-image');

gulp.task('images', () => {
    return gulp.src('./app/Resources/assets/img/**/*')
        .pipe(gulp.dest('./web/assets/images/'))
});

gulp.task('images:build', () => {
    return gulp.src('./app/Resources/assets/img/**/*')
        .pipe(image({
            optipng: false,
            pngquant: ['--speed=1', '--force', 256],
            zopflipng: ['-y', '--lossy_8bit', '--lossy_transparent'],
            jpegRecompress: false,
            mozjpeg: ['-optimize', '-progressive', '-quality', '70'],
            guetzli: false,
            gifsicle: false,
            svgo: false,
            concurrent: 2
        }))
        .pipe(gulp.dest('./web/assets/images/'))
});

gulp.task('webpack', (error) => {
    return run('npm run encore-dev').exec().on('error', () => {
        console.log(error)
    })
});

gulp.task('webpack:build', () => {
    return run('npm run encore-prod').exec()
});
gulp.task('webpack:watch', () => {
    return run('npm run encore-dev-watch').exec()
});

gulp.task('build', gulp.series(
    'webpack:build',
    'images:build'
));

gulp.task('dev', gulp.series(
    'webpack',
    'images'
));

gulp.task('watch', () => {
    gulp.watch('./app/Resources/assets/**/*.*', gulp.series('dev'));
});

gulp.task('default', gulp.series('dev', 'watch'));
// webpack.config.js
var Encore = require('@symfony/webpack-encore');

Encore
// directory where all compiled assets will be stored
    .setOutputPath('web/assets/')

    // what's the public path to this directory (relative to your project's document root dir)
    .setPublicPath('/assets')

    // empty the outputPath dir before each build
    .cleanupOutputBeforeBuild()

    // will output as web/build/app.js
    .addEntry('app', [
        'bootstrap',
        'moment',
        'daterangepicker',
        'adminlte',
        './app/Resources/assets/js/app.js'
    ])

    // will output as web/build/global.css
    .addStyleEntry('style', [
        './node_modules/bootstrap/dist/css/bootstrap.min.css',
        './node_modules/adminlte/dist/css/AdminLTE.css',
        './node_modules/adminlte/dist/css/skins/skin-blue.css',
        './node_modules/font-awesome/css/font-awesome.min.css',
        './node_modules/daterangepicker/daterangepicker.scss',
        './app/Resources/assets/css/main.sass'
    ])

    // allow sass/scss files to be processed
    .enableSassLoader()

    // allow legacy applications to use $/jQuery as a global variable
    .autoProvidejQuery({
        $: 'jquery',
        jQuery: 'jquery'
    })

    .enableSourceMaps(!Encore.isProduction())

    // create hashed filenames (e.g. app.abc123.css)
    .enableVersioning()
;

// export the final configuration
module.exports = Encore.getWebpackConfig();